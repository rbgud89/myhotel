package com.myhotel.joinus;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.myhotel.controllers.Controller;
import com.myhotel.dao.MemberDao;

public class MyReservProcController implements Controller{

	@Override
	public String doController(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String mre_id = request.getParameter("mre_id");
		
		MemberDao mdao = new MemberDao();
		mdao.deleteMyReserv(mre_id);
		
		return "redirect:/myhotel/joinus/myreservation.do";
	}

}
