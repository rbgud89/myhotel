package com.myhotel.joinus;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.myhotel.controllers.Controller;

public class LogoutProcController implements Controller{

	@Override
	public String doController(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String redirect_url = "redirect:"+request.getContextPath()+"/index.do";
			
		request.getSession().invalidate();
		
		Cookie[] cookies = request.getCookies();
		
		for(int i=0; i<cookies.length; i++){
			String cookieName = cookies[i].getName();
			if(cookieName.equals("mid") || cookieName.equals("mname")){
				cookies[i].setMaxAge(0);
				cookies[i].setPath("/");
				response.addCookie(cookies[i]);
			}
		}
		
		return redirect_url;
	}
	
}
