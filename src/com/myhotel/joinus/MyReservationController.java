package com.myhotel.joinus;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.myhotel.controllers.Controller;
import com.myhotel.dao.MemberDao;
import com.myhotel.vo.MyReservation;

public class MyReservationController implements Controller{

	@Override
	public String doController(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String mid = (String)request.getSession().getAttribute("mid");
		System.out.println("mid:"+mid);
		MemberDao mDao = new MemberDao();
		ArrayList<MyReservation> mrlist = mDao.getMyReservation(mid);
		
		System.out.println(mrlist.size());
		request.setAttribute("mrlist", mrlist);
		
		System.out.println("MyReservationController.do");
		return "myreservation.jsp";
	}

}
