package com.myhotel.joinus;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.myhotel.controllers.Controller;

public class LoginController implements Controller{

	@Override
	public String doController(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String error = request.getParameter("error");
		
		if(error != null && !error.equals("")){
			if(error.equals("midx")){
				request.setAttribute("midx", "존재하지 않는 아이디 입니다.");
			} else if(error.equals("pwdx")){
				request.setAttribute("pwdx", "비밀번호가 맞지 않습니다.");
			}
			
		}
		return "login.jsp";
	}

}
