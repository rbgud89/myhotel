package com.myhotel.joinus;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.myhotel.controllers.Controller;
import com.myhotel.dao.MemberDao;
import com.myhotel.vo.Member;

public class LoginProcController implements Controller{

	@Override
	public String doController(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		System.out.println("==LoginProcController==");
		String mid = (String)request.getParameter("mid");
		String pwd = (String)request.getParameter("pwd");
		String autologin = (String)request.getParameter("autologin");
		//chek> on / uncheck> null
		
		System.out.println("mid: "+mid);
		System.out.println("pwd: "+pwd);
		System.out.println("autologin : "+autologin);
		
		MemberDao mdao = new MemberDao();
		Member mem = mdao.getMember(mid);
		
		if(mem == null){
			return "redirect:login.do?error=midx";
		}
		
		String currectPwd = mem.getPwd();
		
		if(!currectPwd.equals(pwd)){
			return "redirect:login.do?error=pwdx";
		}
		
		request.getSession().setAttribute("mid", mid);
		request.getSession().setAttribute("mname", mem.getName());
		
		if(autologin != null){
			if(autologin.equals("on")){
				String name = URLEncoder.encode(mem.getName(),"UTF-8");
				Cookie cookMid = new Cookie("mid",mid);
				Cookie cookName = new Cookie("mname",name);
				
				cookMid.setMaxAge(7*24*60*60);
				cookMid.setPath("/");
				
				cookName.setMaxAge(7*24*60*60);
				cookName.setPath("/");
				
				response.addCookie(cookMid);
				response.addCookie(cookName);
			}
		} else {
			Cookie[] cookies = request.getCookies();
			
			for(int i=0; i<cookies.length; i++){
				String cookieName = cookies[i].getName();
				if(cookieName.equals("mid") || cookieName.equals("mname")){
					cookies[i].setMaxAge(0);
					cookies[i].setPath("/");
					response.addCookie(cookies[i]);
				}
			}
		}
		
		System.out.println("======================");
		System.out.println("dffdf:" + request.getRequestURL());
		return "redirect:/myhotel/index.do";
	}

}
