package com.myhotel.joinus;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.myhotel.controllers.Controller;
import com.myhotel.dao.MemberDao;
import com.myhotel.etc.Util;
import com.myhotel.vo.Member;

public class JoinProcController implements Controller{

	@Override
	public String doController(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		System.out.println("joinproc.jsp");
		String mid = request.getParameter("mid");
		String pwd = request.getParameter("pwd");
		String name = request.getParameter("name");
		String gender = request.getParameter("gender");
		
		String phone_first = request.getParameter("phone_first");
		String phone_middle = request.getParameter("phone_middle");
		String phone_last = request.getParameter("phone_last");
		String phone = ""+phone_first+phone_middle+phone_last;
		
		String email = request.getParameter("email");
		
		
		System.out.println(mid);
		System.out.println(pwd);
		System.out.println(name);
		System.out.println(gender);
		System.out.println(phone_first);
		System.out.println(phone_middle);
		System.out.println(phone_last);
		System.out.println(email);

		MemberDao mDao = new MemberDao();
		Member mem = new Member();
		mem.setMid(mid);
		mem.setPwd(pwd);
		mem.setName(name);
		mem.setGender(gender);
		mem.setPhone(phone);
		mem.setEmail(email);
		mem.setRegdate(new Util().getNowTime());
		
		int changeRow = 0;
		changeRow = mDao.addMember(mem);
		
		if( changeRow == 1){
			response.setContentType("text/html;charset=utf-8");

			PrintWriter out=response.getWriter();
			out.println("alert('가입되었습니다.');");
		}
		
		return "redirect:join_complete.do";
	}

}
