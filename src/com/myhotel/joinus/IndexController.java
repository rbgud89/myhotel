package com.myhotel.joinus;

import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.myhotel.controllers.Controller;

public class IndexController implements Controller{

	@Override
	public String doController(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Cookie[] cookies = request.getCookies();
		
		String cookMid = null;
		String cookName = null;
		
		if(cookies!= null){
			
			for(int i=0; i<cookies.length; i++){
				if(cookies[i].getName().equals("mname")){
					cookName =cookies[i].getValue();
					cookName = URLDecoder.decode(cookName,"UTF-8");
					System.out.println("mname쿠키있음:"+ cookName);
					request.getSession().setAttribute("mname", cookName);
				}
				if(cookies[i].getName().equals("mid")){
					cookMid = cookies[i].getValue();
					System.out.println("mid쿠키있음:"+cookMid);
					request.getSession().setAttribute("mid", cookMid);
				}
			}
		}
		
		return "index.jsp";
	}

}
