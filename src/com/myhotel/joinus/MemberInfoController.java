package com.myhotel.joinus;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.myhotel.controllers.Controller;
import com.myhotel.dao.MemberDao;
import com.myhotel.vo.Member;

public class MemberInfoController implements Controller{

	@Override
	public String doController(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String mid = request.getParameter("mid");
		
		MemberDao mdao = new MemberDao();
		Member mem = mdao.getMember(mid);
		
		JSONObject json = new JSONObject();
		
		if(mem != null){
			json.put("result","exist");
			json.put("point", mem.getPoint());
			
		} else{
			json.put("result","notExist");
		}
		
		response.getWriter().print(json);
		return "ajax:";
		
	}

}
