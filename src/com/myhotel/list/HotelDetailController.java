package com.myhotel.list;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.myhotel.controllers.Controller;
import com.myhotel.dao.CabinDao;
import com.myhotel.vo.Cabin;

public class HotelDetailController implements Controller{

	@Override
	public String doController(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String checkin = request.getParameter("checkin");
		String checkout = request.getParameter("checkout");
		
		String st_id = request.getParameter("st_id");
		String st_name = request.getParameter("st_name");
		String st_address = request.getParameter("st_address");
		String st_phone = request.getParameter("st_phone");
		String avg_score = request.getParameter("avg_score");
		String re_count = request.getParameter("re_count");
		String usedate = request.getParameter("usedate");
		
		String st_lat = request.getParameter("st_lat");
		String st_long = request.getParameter("st_long");
		
		System.out.println("checkin:"+checkin);
		System.out.println("checkout:"+checkout);
		System.out.println("st_id:"+st_id);
		System.out.println("st_name:"+st_name);
		System.out.println("st_address:"+st_address);
		System.out.println("st_phone:"+st_phone);
		System.out.println("avg_score:"+avg_score);
		System.out.println("re_count:"+re_count);
		System.out.println("st_lat:"+st_lat);
		System.out.println("st_long:"+st_long);
		
		CabinDao caDao = new CabinDao();
		ArrayList<Cabin> cabinlist =  caDao.getCabinList(st_id, checkin, checkout);
		for(Cabin ca : cabinlist){
			System.out.println(ca.toString());
		}
		
		request.setAttribute("checkin", checkin);
		request.setAttribute("checkout", checkout);
		request.setAttribute("st_id", st_id);
		request.setAttribute("st_name", st_name);
		request.setAttribute("st_address", st_address);
		request.setAttribute("st_phone", st_phone);
		request.setAttribute("avg_score", avg_score);
		request.setAttribute("re_count", re_count);
		request.setAttribute("usedate", usedate);
		request.setAttribute("st_lat", st_lat);
		request.setAttribute("st_long", st_long);
		request.setAttribute("cabinlist", cabinlist);
		
		return "hoteldetail.jsp";
	}
	
}
