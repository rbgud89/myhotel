package com.myhotel.list;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.myhotel.controllers.Controller;
import com.myhotel.dao.StayDao;
import com.myhotel.vo.Area_latlng;
import com.myhotel.vo.Stay;

public class HotelSearchController implements Controller{

	@Override
	public String doController(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//http://localhost/myhotel/list/hotelsearch.do?
		//st_type=HOTEL&fast_area=a5&checkin=2018-10-25&checkout=2018-10-30
		
		String st_type = request.getParameter("st_type");
		String st_area_code = request.getParameter("st_area_code");
		String checkin = request.getParameter("checkin");
		String checkout = request.getParameter("checkout");
		String st_sort = request.getParameter("st_sort");
		String usedate = request.getParameter("usedate");
		
		String start_price = request.getParameter("start_price");
		String end_price = request.getParameter("end_price");
		String check_rev = request.getParameter("check_rev");

		double start_lat;
		double start_lng;
		int i_start_price ;
		int i_end_price;
		
		if(check_rev != null){
			check_rev = "only_rev_list";
		} else {
			check_rev = "all_list";
		}
		
		if( start_price != null && !(start_price.equals(""))){
			i_start_price = Integer.parseInt(start_price);
		} else {
			i_start_price =1;
		}
		
		if( end_price != null && !(end_price.equals(""))){

			i_end_price = Integer.parseInt(end_price);
		} else{
			i_end_price = 100;
		}
		
	
		StayDao stdao = new StayDao();
		Area_latlng al = stdao.getAreaLatlng(st_area_code);
		
		if(al == null){
			start_lat= 37.566826005485716;
			start_lng = 126.9786567859313;
		} else {
			start_lat = al.getSt_lat();
			start_lng = al.getSt_long();
		}
		
		usedate = (Integer.parseInt(usedate) <=0) ? "1" : usedate;
		
		ArrayList<Stay> stayList = stdao.getStayList(checkin, checkout, st_area_code, st_type, st_sort, i_start_price, i_end_price, usedate, check_rev);
		for(Stay stay : stayList){
			System.out.println(stay.toString());
		}
		
		request.setAttribute("st_type", st_type);
		request.setAttribute("st_area_code", st_area_code);
		request.setAttribute("checkin", checkin);
		request.setAttribute("checkout", checkout);
		request.setAttribute("stayList", stayList);
		request.setAttribute("st_sort", st_sort);
		request.setAttribute("usedate", usedate);
		request.setAttribute("start_price", i_start_price);
		request.setAttribute("end_price", i_end_price);
		request.setAttribute("check_rev", check_rev);
		request.setAttribute("start_lat", start_lat);
		request.setAttribute("start_lng", start_lng);
		return "hotelsearch.jsp";
	}

}
