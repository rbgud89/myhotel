package com.myhotel.list;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.myhotel.controllers.Controller;
import com.myhotel.dao.MemberDao;
import com.myhotel.dao.ReservationDao;
import com.myhotel.etc.Util;
import com.myhotel.vo.MemReserve;

public class ReservProcController implements Controller{

	@Override
	public String doController(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		

		String mid = (String)request.getSession().getAttribute("mid");
		String st_id = request.getParameter("st_id");
		String ca_idx = request.getParameter("ca_idx");
		String checkin = request.getParameter("checkin");
		String checkout	= request.getParameter("checkout");
		String enter_time = request.getParameter("enter_time");
		String out_time = request.getParameter("out_time");
		String price = request.getParameter("price");
		String redate = new Util().getNowTimeAll();
		String mre_id = mid+redate;
		
		int iprice = Integer.parseInt(price);
		
		MemReserve memreserv = new MemReserve();
		memreserv.setMre_id(mre_id);
		memreserv.setMid(mid);
		memreserv.setSt_id(st_id);
		memreserv.setCa_idx(ca_idx);
		memreserv.setCheckin(checkin);
		memreserv.setCheckout(checkout);
		memreserv.setEnter_time(Integer.parseInt(enter_time));
		memreserv.setOut_time(Integer.parseInt(out_time));
		memreserv.setPrice(Integer.parseInt(price));
		memreserv.setRe_date(redate);

		System.out.println("mid:"+mid);
		System.out.println("st_id:"+st_id);
		System.out.println("ca_idx:"+ca_idx);
		System.out.println("checkin:"+checkin);
		System.out.println("checkout:"+checkout);
		System.out.println("enter_time:"+enter_time);
		System.out.println("out_time:"+out_time);
		System.out.println("price:"+price);
		System.out.println("redate:"+redate);

		int changeRow = 0;
		
		MemberDao mDao = new MemberDao();
		int mPoint = mDao.getMember(mid).getPoint();
		
		if(mPoint >= iprice){
			changeRow += mDao.updatePoint(mid, (mPoint-iprice));
		} else {
			System.out.println("금액 부족");
			return "";
		}
		
		if(changeRow != 1){
			System.out.println("금액 변경 실패");
			return "";
		}
		
		ReservationDao reDao = new ReservationDao();
		changeRow += reDao.addReserv(memreserv);
		
		
		if(changeRow == 2){
			return "redirect:/myhotel/index.do";
		} else {
			System.out.println("예약 문제발생");
			return "";
		}
		
	}

}
