package com.myhotel.list;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.myhotel.controllers.Controller;
import com.myhotel.dao.ReservationDao;
import com.myhotel.vo.Reservation;

public class ReservationController implements Controller{

	@Override
	public String doController(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String checkin = request.getParameter("checkin");
		String checkout = request.getParameter("checkout");
		String st_id = request.getParameter("st_id");
		String ca_idx = request.getParameter("ca_idx");
		String usedate = request.getParameter("usedate");

		System.out.println("checkin:"+checkin);
		System.out.println("checkout:"+checkout);
		System.out.println("st_id:"+st_id);
		System.out.println("ca_idx:"+ca_idx);
		
		ReservationDao rdao = new ReservationDao();
		
		Reservation reserv = rdao.getReservation(ca_idx);
		
		request.setAttribute("checkin", checkin);
		request.setAttribute("checkout", checkout);
		request.setAttribute("st_id", st_id);
		request.setAttribute("ca_idx", ca_idx);
		request.setAttribute("usedate", usedate);
		request.setAttribute("reserv", reserv);
		
		return "reservation.jsp";
	}

}
