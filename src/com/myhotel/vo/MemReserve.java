package com.myhotel.vo;

public class MemReserve {

	private String mre_id;
	private String mid;
	private String st_id;
	private String ca_idx;
	private String checkin;
	private String checkout;
	private int enter_time;
	private int out_time;
	private int price;
	private String re_date;
	
	@Override
	public String toString() {
		return "MemReserve [mre_id=" + mre_id + ", mid=" + mid + ", st_id=" + st_id + ", ca_idx=" + ca_idx
				+ ", checkin=" + checkin + ", checkout=" + checkout + ", enter_time=" + enter_time + ", out_time="
				+ out_time + ", price=" + price + ", re_date=" + re_date + "]";
	}
	public String getMre_id() {
		return mre_id;
	}
	public void setMre_id(String mre_id) {
		this.mre_id = mre_id;
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getSt_id() {
		return st_id;
	}
	public void setSt_id(String st_id) {
		this.st_id = st_id;
	}
	public String getCa_idx() {
		return ca_idx;
	}
	public void setCa_idx(String ca_idx) {
		this.ca_idx = ca_idx;
	}
	public String getCheckin() {
		return checkin;
	}
	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}
	public String getCheckout() {
		return checkout;
	}
	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}
	public int getEnter_time() {
		return enter_time;
	}
	public void setEnter_time(int enter_time) {
		this.enter_time = enter_time;
	}
	public int getOut_time() {
		return out_time;
	}
	public void setOut_time(int out_time) {
		this.out_time = out_time;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getRe_date() {
		return re_date;
	}
	public void setRe_date(String re_date) {
		this.re_date = re_date;
	}
	
	
}
