package com.myhotel.vo;

public class MyReservation {
	// MRE_ID, MID, A.ST_ID, A.CA_IDX, CHECKIN, CHECKOUT, 
	//ENTER_TIME, OUT_TIME, PRICE, RE_DATE, ST_NAME, ST_PHONE, 
	//ST_ADDRESS, ST_IMG, CA_NAME, CA_PERSONS 
	
	private String mre_id;
	private String st_id;
	private String ca_idx;
	private String checkin;
	private String checkout;
	private int enter_time;
	private int out_time;
	private int price;
	private String re_date;
	private String st_name;
	private String st_phone;
	private String st_address;
	private String ca_img;
	private String ca_name;
	private int ca_persons;
	@Override
	public String toString() {
		return "MyReservation [mre_id=" + mre_id + ", st_id=" + st_id + ", ca_idx=" + ca_idx + ", checkin=" + checkin
				+ ", checkout=" + checkout + ", enter_time=" + enter_time + ", out_time=" + out_time + ", price="
				+ price + ", re_date=" + re_date + ", st_name=" + st_name + ", st_phone=" + st_phone + ", st_address="
				+ st_address + ", ca_img=" + ca_img + ", ca_name=" + ca_name + ", ca_persons=" + ca_persons + "]";
	}
	public String getMre_id() {
		return mre_id;
	}
	public void setMre_id(String mre_id) {
		this.mre_id = mre_id;
	}
	public String getSt_id() {
		return st_id;
	}
	public void setSt_id(String st_id) {
		this.st_id = st_id;
	}
	public String getCa_idx() {
		return ca_idx;
	}
	public void setCa_idx(String ca_idx) {
		this.ca_idx = ca_idx;
	}
	public String getCheckin() {
		return checkin;
	}
	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}
	public String getCheckout() {
		return checkout;
	}
	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}
	public int getEnter_time() {
		return enter_time;
	}
	public void setEnter_time(int enter_time) {
		this.enter_time = enter_time;
	}
	public int getOut_time() {
		return out_time;
	}
	public void setOut_time(int out_time) {
		this.out_time = out_time;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getRe_date() {
		return re_date;
	}
	public void setRe_date(String re_date) {
		this.re_date = re_date;
	}
	public String getSt_name() {
		return st_name;
	}
	public void setSt_name(String st_name) {
		this.st_name = st_name;
	}
	public String getSt_phone() {
		return st_phone;
	}
	public void setSt_phone(String st_phone) {
		this.st_phone = st_phone;
	}
	public String getSt_address() {
		return st_address;
	}
	public void setSt_address(String st_address) {
		this.st_address = st_address;
	}
	public String getCa_img() {
		return ca_img;
	}
	public void setCa_img(String ca_img) {
		this.ca_img = ca_img;
	}
	public String getCa_name() {
		return ca_name;
	}
	public void setCa_name(String ca_name) {
		this.ca_name = ca_name;
	}
	public int getCa_persons() {
		return ca_persons;
	}
	public void setCa_persons(int ca_persons) {
		this.ca_persons = ca_persons;
	}
	
	
	
	
}
