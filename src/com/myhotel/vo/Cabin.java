package com.myhotel.vo;

public class Cabin {
	private String st_id;
	private String ca_idx;
	private String ca_name;
	private int ca_persons;
	private int ca_max_count;
	private int ca_use_time;
	private int ca_enter_time;
	private int ca_out_time;
	private int ca_price1;
	private int ca_price2;
	private String ca_img;
	private String car_date;
	@Override
	public String toString() {
		return "Cabin [st_id=" + st_id + ", ca_idx=" + ca_idx + ", ca_name=" + ca_name + ", ca_persons=" + ca_persons
				+ ", ca_max_count=" + ca_max_count + ", ca_use_time=" + ca_use_time + ", ca_enter_time=" + ca_enter_time
				+ ", ca_out_time=" + ca_out_time + ", ca_price1=" + ca_price1 + ", ca_price2=" + ca_price2 + ", ca_img="
				+ ca_img + ", car_date=" + car_date + "]";
	}
	public String getSt_id() {
		return st_id;
	}
	public void setSt_id(String st_id) {
		this.st_id = st_id;
	}
	public String getCa_idx() {
		return ca_idx;
	}
	public void setCa_idx(String ca_idx) {
		this.ca_idx = ca_idx;
	}
	public String getCa_name() {
		return ca_name;
	}
	public void setCa_name(String ca_name) {
		this.ca_name = ca_name;
	}
	public int getCa_persons() {
		return ca_persons;
	}
	public void setCa_persons(int ca_persons) {
		this.ca_persons = ca_persons;
	}
	public int getCa_max_count() {
		return ca_max_count;
	}
	public void setCa_max_count(int ca_max_count) {
		this.ca_max_count = ca_max_count;
	}
	public int getCa_use_time() {
		return ca_use_time;
	}
	public void setCa_use_time(int ca_use_time) {
		this.ca_use_time = ca_use_time;
	}
	public int getCa_enter_time() {
		return ca_enter_time;
	}
	public void setCa_enter_time(int ca_enter_time) {
		this.ca_enter_time = ca_enter_time;
	}
	public int getCa_out_time() {
		return ca_out_time;
	}
	public void setCa_out_time(int ca_out_time) {
		this.ca_out_time = ca_out_time;
	}
	public int getCa_price1() {
		return ca_price1;
	}
	public void setCa_price1(int ca_price1) {
		this.ca_price1 = ca_price1;
	}
	public int getCa_price2() {
		return ca_price2;
	}
	public void setCa_price2(int ca_price2) {
		this.ca_price2 = ca_price2;
	}
	public String getCa_img() {
		return ca_img;
	}
	public void setCa_img(String ca_img) {
		this.ca_img = ca_img;
	}
	public String getCar_date() {
		return car_date;
	}
	public void setCar_date(String car_date) {
		this.car_date = car_date;
	}
	
	
	
	
}
