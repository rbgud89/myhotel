package com.myhotel.vo;

public class StayRevStatus {
	private String st_id;
	private String str_date;
	private String str_able;
	
	@Override
	public String toString() {
		return "StayRevStatus [st_id=" + st_id + ", str_date=" + str_date + ", str_able=" + str_able + "]";
	}
	public String getSt_id() {
		return st_id;
	}
	public void setSt_id(String st_id) {
		this.st_id = st_id;
	}
	public String getStr_date() {
		return str_date;
	}
	public void setStr_date(String str_date) {
		this.str_date = str_date;
	}
	public String getStr_able() {
		return str_able;
	}
	public void setStr_able(String str_able) {
		this.str_able = str_able;
	}
	
}
