package com.myhotel.vo;

public class Stay {
	private String st_id;
	private String st_area_code;
	private String st_name;
	private String st_type;
	private String st_phone;
	private String st_address;
	private String st_img;
	private double st_long;
	private double st_lat;
	private int ca_price1;
	private int ca_price2;
	private int re_count;
	private double avg_score;
	@Override
	public String toString() {
		return "Stay [st_id=" + st_id + ", st_area_code=" + st_area_code + ", st_name=" + st_name + ", st_type="
				+ st_type + ", st_phone=" + st_phone + ", st_address=" + st_address + ", st_img=" + st_img
				+ ", st_long=" + st_long + ", st_lat=" + st_lat + ", ca_price1=" + ca_price1 + ", ca_price2="
				+ ca_price2 + ", re_count=" + re_count + ", avg_score=" + avg_score + "]";
	}
	public String getSt_id() {
		return st_id;
	}
	public void setSt_id(String st_id) {
		this.st_id = st_id;
	}
	public String getSt_area_code() {
		return st_area_code;
	}
	public void setSt_area_code(String st_area_code) {
		this.st_area_code = st_area_code;
	}
	public String getSt_name() {
		return st_name;
	}
	public void setSt_name(String st_name) {
		this.st_name = st_name;
	}
	public String getSt_type() {
		return st_type;
	}
	public void setSt_type(String st_type) {
		this.st_type = st_type;
	}
	public String getSt_phone() {
		return st_phone;
	}
	public void setSt_phone(String st_phone) {
		this.st_phone = st_phone;
	}
	public String getSt_address() {
		return st_address;
	}
	public void setSt_address(String st_address) {
		this.st_address = st_address;
	}
	public String getSt_img() {
		return st_img;
	}
	public void setSt_img(String st_img) {
		this.st_img = st_img;
	}
	public double getSt_long() {
		return st_long;
	}
	public void setSt_long(double st_long) {
		this.st_long = st_long;
	}
	public double getSt_lat() {
		return st_lat;
	}
	public void setSt_lat(double st_lat) {
		this.st_lat = st_lat;
	}
	public int getCa_price1() {
		return ca_price1;
	}
	public void setCa_price1(int ca_price1) {
		this.ca_price1 = ca_price1;
	}
	public int getCa_price2() {
		return ca_price2;
	}
	public void setCa_price2(int ca_price2) {
		this.ca_price2 = ca_price2;
	}
	public int getRe_count() {
		return re_count;
	}
	public void setRe_count(int re_count) {
		this.re_count = re_count;
	}
	public double getAvg_score() {
		return avg_score;
	}
	public void setAvg_score(double avg_score) {
		this.avg_score = avg_score;
	}
	
	
	
}
