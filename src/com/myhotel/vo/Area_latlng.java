package com.myhotel.vo;

public class Area_latlng {

	public String st_area_code;
	private double st_lat;
	private double st_long;
	
	@Override
	public String toString() {
		return "Area_latlng [st_area_code=" + st_area_code + ", st_lat=" + st_lat + ", st_long=" + st_long + "]";
	}

	public String getSt_area_code() {
		return st_area_code;
	}

	public void setSt_area_code(String st_area_code) {
		this.st_area_code = st_area_code;
	}

	public double getSt_lat() {
		return st_lat;
	}

	public void setSt_lat(double st_lat) {
		this.st_lat = st_lat;
	}

	public double getSt_long() {
		return st_long;
	}

	public void setSt_long(double st_long) {
		this.st_long = st_long;
	}
	
	
	
}
