package com.myhotel.etc;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Util {

	public String getNowTime(){
		SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat ( "yyyy/MM/dd", Locale.KOREA );
		Date currentTime = new Date ();
		String date= mSimpleDateFormat.format ( currentTime );
		
		return date;
	}
	public String getNowTimeAll(){
		SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat ( "yyyy/MM/dd/k/m/s", Locale.KOREA );
		Date currentTime = new Date ();
		String date= mSimpleDateFormat.format ( currentTime );
		
		return date;
	}
}
