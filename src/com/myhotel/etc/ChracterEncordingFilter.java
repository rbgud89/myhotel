package com.myhotel.etc;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class ChracterEncordingFilter implements Filter{

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		//System.out.println("필터 사전 작업");
		request.setCharacterEncoding("UTF-8");
		chain.doFilter(request, response);
		
		//System.out.println("필터 사후 작업");
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		System.out.println("필터 초기vFilterConfig config화");
		//String encordign = config.getInitParameter("encoding");
				
		
	}

}
