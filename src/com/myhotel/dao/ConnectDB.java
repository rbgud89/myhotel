package com.myhotel.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectDB {
	private String url = "jdbc:mariadb://rbgud89.cafe24.com:3306/rbgud89";
	//private String url = "jdbc:mariadb://localhost:3306/rbgud89";
	private String user ="rbgud89";
	private String pwd = "apple123";
	private static ConnectDB connectDB = null;
	
	public Connection getConn(){
		Connection con = null;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			con = DriverManager.getConnection(url,user,pwd);
		} catch (ClassNotFoundException e) {
			System.out.println("오라클 드라이버 로드 실패");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("접속 정보가 잘못되었습니다.");
			e.printStackTrace();
		}
		
		return con;
	}
	
	public synchronized static ConnectDB getInstance(){
		if( connectDB == null ){
			connectDB = new ConnectDB();
		} 
		return connectDB;
	}
}
