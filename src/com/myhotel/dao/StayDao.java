package com.myhotel.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.myhotel.vo.Area_latlng;
import com.myhotel.vo.Member;
import com.myhotel.vo.Stay;

public class StayDao {

	public Area_latlng getAreaLatlng(String st_area_code){
		Connection con = ConnectDB.getInstance().getConn();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Area_latlng al = null;
		
		String sql = "SELECT * FROM HOTEL_AREA_LATLNG WHERE ST_AREA_CODE = ?";
				
		System.out.println(sql);
		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, st_area_code);	
			rs = ps.executeQuery();
			
			if(rs.next()){
				al = new Area_latlng();
				al.setSt_area_code(rs.getString("ST_AREA_CODE"));
				al.setSt_lat(rs.getDouble("ST_LAT"));
				al.setSt_long(rs.getDouble("ST_LONG"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				rs.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return al;
	}
	
	public ArrayList<Stay> getStayList(String checkin, String checkout, String st_area_code, String st_type, String st_sort, int start_price, int end_price, String usedate, String check_rev){
		Connection con = ConnectDB.getInstance().getConn();
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Stay> stayList = null;
		Stay stay = null;
		
		String sql = "SELECT *,A.ST_ID MAIN_ST_ID FROM HOTEL_STAY A LEFT JOIN "
				+ "(SELECT * FROM HOTEL_STAY_REV_STATUS WHERE STR_DATE BETWEEN ? AND ? GROUP BY ST_ID) B "
				+ "ON A.ST_ID = B.ST_ID "
				+ "LEFT JOIN (SELECT ST_ID, CA_PRICE1*"+usedate+" CA_PRICE1, CA_PRICE2*"+usedate+" CA_PRICE2 FROM HOTEL_CABIN GROUP BY ST_ID) C "
				+ "ON A.ST_ID = C.ST_ID "
				+ "LEFT JOIN (SELECT ST_ID,COUNT(*) RE_COUNT,ROUND(AVG(RE_SCORE),1) AVG_SCORE FROM HOTEL_REVIEW GROUP BY ST_ID) D "
				+ "ON A.ST_ID = D.ST_ID "
				+ "WHERE A.ST_AREA_CODE = ? AND ST_TYPE=? AND CA_PRICE2 BETWEEN ? AND ? ";
		
		if(check_rev.equals("only_rev_list")){
			sql += "AND STR_DATE IS NULL";
		}
		
		if(st_sort != null){
			if(st_sort.equals("ST_ID")){
				sql+=" ORDER BY A.ST_ID ASC";
			} else if(st_sort.equals("AVG_SCORE")){
				sql+=" ORDER BY AVG_SCORE DESC";
			}
		}
		System.out.println(sql);
		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, checkin);
			ps.setString(2, checkout);	
			ps.setString(3, st_area_code);	
			ps.setString(4, st_type);
			ps.setInt(5, start_price*10000);
			ps.setInt(6, end_price*10000);
			rs = ps.executeQuery();
			//System.out.println("checkin:"+checkin);
			//System.out.println("checkout:"+checkout);
			//System.out.println("st_area_code:"+st_area_code);
			stayList = new ArrayList<Stay>();
			while(rs.next()){
				stay = new Stay();
				stay.setSt_id(rs.getString("MAIN_ST_ID"));
				stay.setSt_area_code(rs.getString("ST_AREA_CODE"));
				stay.setSt_name(rs.getString("ST_NAME"));
				stay.setSt_type(rs.getString("ST_TYPE"));
				stay.setSt_phone(rs.getString("ST_PHONE"));
				stay.setSt_address(rs.getString("ST_ADDRESS"));
				stay.setSt_img(rs.getString("ST_IMG"));
				stay.setSt_lat(rs.getDouble("ST_LAT"));
				stay.setSt_long(rs.getDouble("ST_LONG"));
				stay.setCa_price1(rs.getInt("CA_PRICE1"));
				stay.setCa_price2(rs.getInt("CA_PRICE2"));
				stay.setRe_count(rs.getInt("RE_COUNT"));
				stay.setAvg_score(rs.getDouble("AVG_SCORE"));
				stayList.add(stay);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				rs.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return stayList;
	}
}
