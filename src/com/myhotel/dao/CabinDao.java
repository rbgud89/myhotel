package com.myhotel.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.myhotel.vo.Cabin;

public class CabinDao {

	public ArrayList<Cabin> getCabinList(String st_id, String checkin, String checkout){
		Connection con = ConnectDB.getInstance().getConn();
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Cabin> cabinlist = null;
		Cabin cabin = null;
		
		String sql = "SELECT * FROM HOTEL_CABIN A LEFT JOIN "
				+ "(SELECT * FROM HOTEL_CABIN_REV_STATUS WHERE CAR_DATE BETWEEN ? AND ? GROUP BY CA_IDX ) B "
				+ "ON A.CA_IDX = B.CA_IDX "
				+ "LEFT JOIN (SELECT * FROM HOTEL_CABIN_IMG GROUP BY ST_ID) C "
				+ "ON A.CA_IDX = C.CA_IDX WHERE A.ST_ID = ?";
		
		System.out.println(sql);
		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, checkin);
			ps.setString(2, checkout);
			ps.setString(3, st_id);
			System.out.println("st_id:"+st_id);
			rs = ps.executeQuery();

			cabinlist = new ArrayList<Cabin>();
			while(rs.next()){
				cabin = new Cabin();
				cabin.setSt_id(rs.getString("ST_ID"));
				cabin.setCa_idx(rs.getString("CA_IDX"));
				cabin.setCa_name(rs.getString("CA_NAME"));
				cabin.setCa_persons(rs.getInt("CA_PERSONS"));
				cabin.setCa_max_count(rs.getInt("CA_MAX_COUNT"));
				cabin.setCa_use_time(rs.getInt("CA_USE_TIME"));
				cabin.setCa_enter_time(rs.getInt("CA_ENTER_TIME"));
				cabin.setCa_out_time(rs.getInt("CA_OUT_TIME"));
				cabin.setCa_price1(rs.getInt("CA_PRICE1"));
				cabin.setCa_price2(rs.getInt("CA_PRICE2"));
				cabin.setCa_img(rs.getString("CA_IMG"));
				cabin.setCar_date(rs.getString("CAR_DATE"));
				cabinlist.add(cabin);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				rs.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return cabinlist;
	}
}
