package com.myhotel.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.myhotel.vo.Member;
import com.myhotel.vo.MyReservation;
import com.myhotel.vo.Reservation;

public class MemberDao {

	public Member getMember(String mid){
		Connection con = ConnectDB.getInstance().getConn();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		Member mem= null;
		
		String sql = "SELECT * FROM HOTEL_MEMBER WHERE MID = ?";
		
		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, mid);
			rs = ps.executeQuery();
			
			if(rs.next()){
				mem = new Member();
				mem.setMid(rs.getString("mid"));
				mem.setPwd(rs.getString("pwd"));
				mem.setName(rs.getString("name"));
				mem.setGender(rs.getString("gender"));
				mem.setPhone(rs.getString("phone"));
				mem.setEmail(rs.getString("email"));
				mem.setRegdate(rs.getString("regdate"));
				mem.setPoint(rs.getInt("point"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				rs.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return mem;
	}
	
	public int addMember(Member mem){
		Connection con = ConnectDB.getInstance().getConn();
		PreparedStatement ps = null;
		int changeRow = 0;
		
		String sql = "INSERT INTO HOTEL_MEMBER(MID, PWD, NAME, GENDER, PHONE, EMAIL, REGDATE) "
				+ "VALUES(?,?,?,?,?,?,?)";
		
		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, mem.getMid());
			ps.setString(2, mem.getPwd());
			ps.setString(3, mem.getName());
			ps.setString(4, mem.getGender());
			ps.setString(5, mem.getPhone());
			ps.setString(6, mem.getEmail());
			ps.setString(7, mem.getRegdate());
			
			changeRow = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return changeRow;
	}
	
	public int updatePoint(String mid, int point){
		Connection con = ConnectDB.getInstance().getConn();
		PreparedStatement ps = null;
		int changeRow = 0;
		
		String sql = "UPDATE HOTEL_MEMBER SET POINT=? WHERE MID=?";
		
		try {
			ps = con.prepareStatement(sql);
			ps.setInt(1, point);
			ps.setString(2, mid);
			changeRow = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return changeRow;
	}
	
	public ArrayList<MyReservation> getMyReservation(String mid){
		Connection con = ConnectDB.getInstance().getConn();
		PreparedStatement ps = null;
		ResultSet rs = null;
		MyReservation mreserv = null;
		ArrayList<MyReservation> mrlist = null;
		
		String sql = "SELECT "
				 + "MRE_ID, MID, A.ST_ID, A.CA_IDX, CHECKIN, CHECKOUT, ENTER_TIME, OUT_TIME, PRICE, RE_DATE, ST_NAME, ST_PHONE, ST_ADDRESS, CA_IMG, CA_NAME, CA_PERSONS " 
				 + "FROM HOTEL_MEM_RESERVE A "
				 + "LEFT JOIN HOTEL_STAY B " 
				 + "ON A.ST_ID = B.ST_ID " 
				 + "LEFT JOIN HOTEL_CABIN C "
				 + "ON A.CA_IDX = C.CA_IDX "
				 + "LEFT JOIN (SELECT * FROM HOTEL_CABIN_IMG GROUP BY ST_ID) D "
				 + "ON A.CA_IDX = D.CA_IDX "
				 + "WHERE MID=? ";
		
		
		System.out.println(sql);
		try {
			ps = con.prepareStatement(sql);
			
			ps.setString(1, mid);

			rs = ps.executeQuery();
			mrlist = new ArrayList<MyReservation>();
			while(rs.next()){
				System.out.println("ttt");
				mreserv = new MyReservation();
				mreserv.setMre_id(rs.getString("MRE_ID"));
				mreserv.setSt_id(rs.getString("ST_ID"));
				mreserv.setCa_idx(rs.getString("CA_IDX"));
				mreserv.setCheckin(rs.getString("CHECKIN"));
				mreserv.setCheckout(rs.getString("CHECKOUT"));
				mreserv.setEnter_time(rs.getInt("ENTER_TIME"));
				mreserv.setOut_time(rs.getInt("OUT_TIME"));
				mreserv.setPrice(rs.getInt("PRICE"));
				mreserv.setRe_date(rs.getString("RE_DATE"));
				mreserv.setSt_name(rs.getString("ST_NAME"));
				mreserv.setSt_phone(rs.getString("ST_PHONE"));
				mreserv.setSt_address(rs.getString("ST_ADDRESS"));
				mreserv.setCa_img(rs.getString("CA_IMG"));
				mreserv.setCa_name(rs.getString("CA_NAME"));
				mreserv.setCa_persons(rs.getInt("CA_PERSONS"));
				mrlist.add(mreserv);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				rs.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return mrlist;
	}
	
	public int deleteMyReserv(String mre_id){
		Connection con = ConnectDB.getInstance().getConn();
		PreparedStatement ps = null;
		int changeRow = 0;
		
		String sql = "DELETE FROM HOTEL_MEM_RESERVE WHERE MRE_ID=?";
		
		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, mre_id);
			changeRow = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return changeRow;
	}
}
