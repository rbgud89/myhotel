package com.myhotel.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.myhotel.vo.MemReserve;
import com.myhotel.vo.Reservation;

public class ReservationDao {
	
	public Reservation getReservation(String ca_idx){
		Connection con = ConnectDB.getInstance().getConn();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Reservation reserv = null;
		
		String sql = "SELECT A.ST_ID, ST_NAME, ST_PHONE, ST_ADDRESS, B.CA_IDX, CA_NAME, CA_PERSONS, CA_USE_TIME, CA_ENTER_TIME, CA_OUT_TIME, CA_PRICE1, CA_PRICE2, CA_IMG "
				+ "FROM HOTEL_STAY A "
				+ "LEFT JOIN HOTEL_CABIN "
				+ "B ON A.ST_ID = B.ST_ID "
				+ "LEFT JOIN (SELECT * FROM HOTEL_CABIN_IMG WHERE CA_IDX = ? GROUP BY CA_IDX) C "
				+ "ON B.CA_IDX = C.CA_IDX "
				+ "WHERE B.CA_IDX = ? ";

		
		System.out.println(sql);
		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, ca_idx);
			ps.setString(2, ca_idx);
			System.out.println("ca_idx:"+ca_idx);
			rs = ps.executeQuery();

			if(rs.next()){
				reserv = new Reservation();
				reserv.setSt_id(rs.getString("ST_ID"));
				reserv.setSt_name(rs.getString("ST_NAME"));
				reserv.setSt_phone(rs.getString("ST_PHONE"));
				reserv.setSt_address(rs.getString("ST_ADDRESS"));
				reserv.setCa_idx(rs.getString("CA_IDX"));
				reserv.setCa_name(rs.getString("CA_NAME"));
				reserv.setCa_persons(rs.getInt("CA_PERSONS"));
				reserv.setCa_use_time(rs.getInt("CA_USE_TIME"));
				reserv.setCa_enter_time(rs.getInt("CA_ENTER_TIME"));
				reserv.setCa_out_time(rs.getInt("CA_OUT_TIME"));
				reserv.setCa_price1(rs.getInt("CA_PRICE1"));
				reserv.setCa_price2(rs.getInt("CA_PRICE2"));
				reserv.setCa_img(rs.getString("CA_IMG"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				rs.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return reserv;
	}
	
	public int addReserv(MemReserve mr){
		Connection con = ConnectDB.getInstance().getConn();
		PreparedStatement ps = null;
		int changeRow = 0;
		
		String sql = "INSERT INTO HOTEL_MEM_RESERVE VALUES(?,?,?,?,?,?,?,?,?,?)";
		
		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, mr.getMre_id());
			ps.setString(2, mr.getMid());
			ps.setString(3, mr.getSt_id());
			ps.setString(4, mr.getCa_idx());
			ps.setString(5, mr.getCheckin());
			ps.setString(6, mr.getCheckout());
			ps.setInt(7, mr.getEnter_time());
			ps.setInt(8, mr.getOut_time());
			ps.setInt(9, mr.getPrice());
			ps.setString(10, mr.getRe_date());
			
			changeRow = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return changeRow;
	}
}
