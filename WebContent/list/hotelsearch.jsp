<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>숙소리스트</title>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=e93e0b6aa39c656ccbb8d8fab1db0972"></script>
<link href="../style/hotelsearch.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../style/popup.css" >
<style>
	/* calendar popup posiction*/
	.one_calender_box{
		position: fixed;
		top: 155px;
    	left: 30%;
    	z-index: 999;
    	display: none;
	}
</style>
</head>
<script>
$(document).ready(function(){
	
	//popup item class name change for item text color change
	if($("#st_sort").val() == "ST_ID"){
		$("#li_sort>span").text("기본순");
		$("#btn_sort_nomal").attr('class','input_radio radio_activity');
		$("#btn_sort_avg").attr('class','input_radio');
	} else if($("#st_sort").val() == "AVG_SCORE"){
		$("#li_sort>span").text("평점순");
		$("#btn_sort_nomal").attr('class','input_radio');
		$("#btn_sort_avg").attr('class','input_radio radio_activity');
	}
	
	//popup hide events
	$(document).mouseup(function (e){
		console.log("1");
		var container = $("#one_calender_box");
		var container2 = $("#popup01");
		var container3 = $("#popup02");
		if( container.has(e.target).length === 0){
			container.hide();
		}
		if( container2.has(e.target).length === 0){
			container2.hide();
		}
		if( container3.has(e.target).length === 0){
			container3.hide();
		}
		
	});
	
	//popups show events
	$("#li_sort").click(function(){
		$("#popup01").show();
	});
	$("#li_price").click(function(){
		$("#popup02").show();
	});
	
	//ppups item click events
	$("#btn_sort_nomal").click(function(){
		$("#st_sort").val('ST_ID');
		$("#search_form").submit();
	});
	
	$("#btn_sort_avg").click(function(){
		$("#st_sort").val('AVG_SCORE');
		$("#search_form").submit();
	});
	
	$("#btn_sort_price").click(function(){
		$("#search_form").submit();
	});
	
	 $("#check_rev").change(function(){
        $("#search_form").submit();
    });

	$("#btn_init_filter").click(function(){
		$("#check_rev").prop("checked", false);
		$("#st_sort").val('ST_ID');
		$("#start_price").val("1")
		$("#end_price").val("100");
		$("#search_form").submit();
	});
	
})

</script>
<body>
	<jsp:include page="../header.jsp"></jsp:include>
	
	<form id="search_form" action="/myhotel/list/hotelsearch.do" method="GET">
	<input type="hidden" id="st_type" name="st_type" value="${st_type}">
	<input type="hidden" id="st_area_code" name="st_area_code" value="${st_area_code}">
	<input type="hidden" id="st_sort" name="st_sort" value="${st_sort}">
	<input type="hidden" id="search_usedate" name="usedate" value="${usedate}">
	<div style="clear: both"></div>
	<div class="hs_haeder">
		<div class="calenderbar">
			<div class="calender_box">
				<div class="calenderbar_img">
					<img id="calender_btn" onclick="show_calendar()" src="../img/icon/Calender.png">
				</div>
				<div class="Calender_date">
					<input type="text" id="search_checkin" name="checkin" value="${checkin}"readonly> 
					<img class="img_arrow" src="../img/icon/arrow.png">
					<input type="text" id="search_checkout" name="checkout" value="${checkout}"readonly>
					<img class="img_check" src="../img/icon/check.png">
					<h2 id="stay_dates">1박2일</h2>
				</div>
			</div>
		</div>
		
		<div style="clear: both"></div>
		<div class="searchbar">
			<div class="stay_type_box">
				<ul>
					<li id="type_motel"><span>모텔</span></li>
					<li id="type_hotel"><span>호텔</span></li>
					<li id="type_pension"><span>펜션</span></li>
				</ul>
			</div>
			<div class="col_line"></div>
			<div class="search_option_box">
				<ul>
					<li id="li_sort"><span>기본순</span> <img src="../img/icon/filter.png">
						<div id="popup01" class="popup_type">
						    <div class="popup_type_box">
						        <div id="btn_sort_nomal" class="input_radio">기본순</div>
						        <div id="btn_sort_avg" class="input_radio" >평점순</div>
						    </div>
						</div>
					</li>
					<li>테마 <img src="../img/icon/filter.png"></li>
					<li id="li_price">가격선택 <img src="../img/icon/filter.png">
						<div id="popup02" class="popup_type">
						    <div class="popup_type_box">
						    	<input id="start_price" name="start_price" type="number" value="${start_price}">만원<br>
						    	~<input id="end_price" name="end_price"type="number" value="${end_price}">만원<br>
						    	<button id="btn_sort_price" type="button">확인</button>
						    </div>
						</div>
					</li>
					<li>
						<c:choose>
							<c:when test="${check_rev == 'only_rev_list'}">
								<input type="checkbox" id="check_rev" name="check_rev" checked/> 예약가능
							</c:when>
							<c:when test="${check_rev == 'all_list'}">
								<input type="checkbox" id="check_rev" name="check_rev"/> 예약가능
							</c:when>
							<c:otherwise>
								<input type="checkbox" id="check_rev" name="check_rev"/> 예약가능
							</c:otherwise>
						</c:choose>
					</li>
				</ul>
			</div>
			<div class="col_line"></div>
			<div class="init_box">
				<button class="initbtn" type="button">
					<img src="../img/icon/filter2.png">
					<div id="btn_init_filter">필터 초기화</div>
				</button>
			</div>
		</div>
		<div class="hs_searchbar_line" style="clear: both"></div>
	</div>
	</form>
	
	<div class="hs_haeder_blank"></div>
	<div class="wrap">
		<div class="stay_list_box">
				
			
			<!-- stay_list start -->
			<c:forEach var='stay' items='${stayList}'>
				<input type="hidden" class="mapinfos" value="${stay.st_name}/${stay.st_lat}/${stay.st_long}">
			</c:forEach>
			<ul class="stay_list">
			<c:choose>
				<c:when test="${fn:length(stayList) <= 0}">
					<div class="hs_nodata_box">
						<h1>데이터가 없어요.</h1>
					</div>
					
				</c:when>
				<c:otherwise>
					<c:forEach var='stay' items='${stayList}' varStatus="i">
				<li>
					<div id="stay_offset${i.index}">
					<div class="stay_box">
						<div class="stay_img_box">
							<img src="../img/staymain/${stay.st_img}">
						</div>
						<div class="stay_content">
							<div class="content_box">
								<h1 class="stay_title">${stay.st_name}</h1>
								<div class="stay_info">
									<div class="stay_col1">
										<ul class="stay_start">
											<li><img src="../img/icon/star.png"></li>
											<li><img src="../img/icon/star.png"></li>
											<li><img src="../img/icon/star.png"></li>
											<li><img src="../img/icon/star.png"></li>
											<li><img src="../img/icon/star.png"></li>
										</ul>
										<div style="clear: both"></div>
										<div class="evgscore_img">
											<h1>${stay.avg_score}</h1>
											<img src="../img/icon/score_back.png">
										</div>
									</div>
									<div class="stay_col2">
										<div class="stay_address">
											<h1>${stay.st_address}</h1>
										</div>
										<div class="score_ment">
											<c:choose>
												<c:when test="${stay.avg_score >= 9}">
													<h1>강력 추천!</h1>
												</c:when>
												<c:when test="${stay.avg_score >= 8}">
													<h1>추천!</h1>
												</c:when>
												<c:when test="${stay.avg_score >= 5}">
													<h1>나쁘지않아요!</h1>
												</c:when>
												<c:otherwise>
													<h1>괜찮아요..!</h1>
												</c:otherwise>
											</c:choose>
											
										</div>
										<div style="clear: both;"></div>
										<div class="review_count">
											<h1>후기: ${stay.re_count}개</h1>
										</div>
									</div>
								</div>
								<div class="content_tema_box"></div>
							</div>
							<div class="price_box">
								<div class="price_ment_box">
									<h1>20대가 가장좋아하는 호텔</h1>
								</div>
								<c:choose>
									<c:when test="${usedate ==0 || usedate ==1 }">
										<div class="price_day_box">
											<label>대실</label>
											<label><fmt:formatNumber value="${stay.ca_price1}" pattern="#,###"/>원</label>
										</div>
										<div class="price_day_box">
											<label>숙박</label>
											<label><fmt:formatNumber value="${stay.ca_price2}" pattern="#,###"/>원</label>
										</div>
									</c:when>
									<c:otherwise>
										<div class="price_day_box">
											<label>숙박</label>
											<label><fmt:formatNumber value="${stay.ca_price2}" pattern="#,###"/>원</label>
										</div>
									</c:otherwise>
								</c:choose>
								<div class="price_infobtn_box">
									<form action="/myhotel/list/hoteldetail.do" method="POST">
										<input type="hidden" name="checkin" value="${checkin}">
										<input type="hidden" name="checkout" value="${checkout}">
										<input type="hidden" name="st_id" value="${stay.st_id}">
										<input type="hidden" name="st_name" value="${stay.st_name}">
										<input type="hidden" name="st_address" value="${stay.st_address}">
										<input type="hidden" name="st_phone" value="${stay.st_phone}">
										<input type="hidden" name="avg_score" value="${stay.avg_score}">
										<input type="hidden" name="re_count" value="${stay.re_count}">
										<input type="hidden" name="st_lat" value="${stay.st_lat}">
										<input type="hidden" name="st_long" value="${stay.st_long}">
										<input type="hidden" id="usedate" name="usedate" value="${usedate}">
										<button type="submit" >세부 정보 보기</button>
									</form>
									
								</div>
							</div>
						</div>
					</div>
				</li>
				</c:forEach>
				</c:otherwise>
			</c:choose>
			</ul>
			<!-- stay_list end -->
		</div>
		<div id="map" class="map">
		<div style="z-index: 9999; position: absolute;"><img src="/myhotel/img/trans_square.png"></div>
		</div>
	</div>
	<div style="clear: both"></div>
	<jsp:include page="../footer.jsp"></jsp:include>
	<jsp:include page="../calender.jsp"></jsp:include>
	
	<script>
		
		var positions = [];

		var mapinfos = document.getElementsByClassName("mapinfos");
		for(var i=0; i<mapinfos.length; i++){

			var map_info  = String(mapinfos[i].value).split('/');
			console.log('lat:'+map_info[1] +'/' + 'lng:'+map_info[2])
			var test = {
				content: '<div>'+map_info[0]+'</div>', 
				latlng: new daum.maps.LatLng(map_info[1], map_info[2])
			}
			positions.push(test);
			//addMarker(new daum.maps.LatLng(myLatLng[0], myLatLng[1]));
		}
		var start_lat = ${start_lat};
		var start_lng = ${start_lng};
		
		console.log(start_lat);
		console.log(start_lng);
		
		var mapContainer = document.getElementById('map'), // 지도를 표시할 div  
		mapOption = { 
			center: new daum.maps.LatLng(start_lat, start_lng), // 지도의 중심좌표
			level: 6 // 지도의 확대 레벨
		};

		var map = new daum.maps.Map(mapContainer, mapOption); // 지도를 생성합니다
		

		// 마커 이미지의 이미지 주소입니다
		var imageSrc = "/myhotel/img/icon/position.png"; 
			
		for (var i = 0; i < positions.length; i ++) {
			
			// 마커 이미지의 이미지 크기 입니다
			var imageSize = new daum.maps.Size(24, 35); 
			
			// 마커 이미지를 생성합니다    
			var markerImage = new daum.maps.MarkerImage(imageSrc, imageSize); 
			
			// 마커를 생성합니다
			var marker = new daum.maps.Marker({
				map: map, // 마커를 표시할 지도
				position: positions[i].latlng, // 마커를 표시할 위치
				title : positions[i].title, // 마커의 타이틀, 마커에 마우스를 올리면 타이틀이 표시됩니다
				image : markerImage // 마커 이미지 
			});

			var infowindow = new daum.maps.InfoWindow({
        		content: positions[i].content // 인포윈도우에 표시할 내용
    		});

			// 마커에 mouseover 이벤트와 mouseout 이벤트를 등록합니다
			// 이벤트 리스너로는 클로저를 만들어 등록합니다 
			// for문에서 클로저를 만들어 주지 않으면 마지막 마커에만 이벤트가 등록됩니다
			daum.maps.event.addListener(marker, 'mouseover', makeOverListener(map, marker, infowindow));
			daum.maps.event.addListener(marker, 'mouseout', makeOutListener(infowindow));
			daum.maps.event.addListener(marker, 'click', makeClickListener(i));
		}

		// 인포윈도우를 표시하는 클로저를 만드는 함수입니다 
		function makeOverListener(map, marker, infowindow) {
			return function() {
				infowindow.open(map, marker);
			};
		}

		// 인포윈도우를 닫는 클로저를 만드는 함수입니다 
		function makeOutListener(infowindow) {
			return function() {
				infowindow.close();
			};
		}

		function makeClickListener(index){
			return function(){
				 var offset = $("#stay_offset" + index).offset();
        		$('html, body').animate({scrollTop : offset.top-200}, 100);
			}	
		}

	</script>
	<script>
		//숙소 타입 선택 이벤트
		var type_motel = document.getElementById("type_motel").getElementsByTagName('span')[0];
		var type_hotel = document.getElementById("type_hotel").getElementsByTagName('span')[0];
		var type_pension = document.getElementById("type_pension").getElementsByTagName('span')[0];
		var st_type = document.getElementById("st_type");
		
		if(st_type.value == 'MOTEL'){
			stay_type_option("MOTEL", "4px solid #ff7200", "0", "0");
		} else if(st_type.value =='HOTEL'){
			stay_type_option("HOTEL", "0", "4px solid #ff7200", "0");
		} else if(st_type.value =='PENS'){
			stay_type_option("PENS", "0", "0", "4px solid #ff7200");
		}
		
		function stay_type_option(val, b1, b2, b3) {
			st_type.value = val;
			type_motel.style.borderBottom  = b1;
			type_hotel.style.borderBottom = b2;
			type_pension.style.borderBottom = b3;
		}

		type_motel.onclick = function() {
			stay_type_option("MOTEL", "4px solid #ff7200", "0", "0");
			document.getElementById("search_form").submit();
		}
		type_hotel.onclick = function() {
			stay_type_option("HOTEL", "0", "4px solid #ff7200", "0");
			document.getElementById("search_form").submit();
		}
		type_pension.onclick = function() {
			stay_type_option("PENS", "0", "0", "4px solid #ff7200");
			document.getElementById("search_form").submit();
		}
	</script>
	
	<script type="text/javascript">		
		var usedate;
	
		usedate = Number(document.getElementById('search_usedate').value);
		document.getElementById("stay_dates").innerHTML= (usedate)+'박'+(usedate+1)+'일';
	
		var show_calendar = function(){
			document.getElementById("one_calender_box").style.display="block";
		}
		
		var makeBetweenDate = function(date1, date2){
			var date1 = new Date(''+date1);
			var date2 = new Date(''+date2);
			var diff = (date2.getTime() - date1.getTime()) / (1000*60*60*24);
			return diff;
		}
		
		document.getElementById("cal_search_btn").onclick = function(){
			var checkin_value= document.getElementById("checkin").value;
			var checkout_value= document.getElementById("checkout").value;
			
			document.getElementById("one_calender_box").style.display ="none";
			
			document.getElementById("search_checkin").value = checkin_value;
			document.getElementById("search_checkout").value = checkout_value;
			
			document.getElementById("search_usedate").value = makeBetweenDate(checkin_value, checkout_value);
			document.getElementById("search_form").submit();
		}
		
	</script>
</body>

</html>