<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" media="screen" href="../style/reservation.css" />
</head>
<body>

    <jsp:include page="../header.jsp"></jsp:include>
    <div class="warp">
    	<input type="hidden" id="search_usedate" value="${usedate}">
        <div class="reser_cal1">
            <div class="reser_cal1_cal1">
                <img src="${pageContext.request.contextPath}/img/${st_id}/${reserv.ca_img}">
            </div>
            <div class="reser_cal1_cal2">
                <h1 class="room_title">${reserv.ca_name}</h1>
                <h1 class="stay_name">${reserv.st_name}</h1>
                <h1 class="address">${reserv.st_address}</h1>
                <h1 class="room_person">기준 ${reserv.ca_persons}명</h1>
            </div>

            <div style="clear: both;"></div>
            <div class="reser_tab_box">
                <div class="reser_tab" onclick="click_tab1()">
                    <ul class="tab_contents">
                        <li>
                            <div class="circle1">
                                <img class="cir_checked" src="../img/icon/check_reserv.png">
                            </div>
                        </li>
                        <li>대실</li>
                        <li><span> 예약가능 </span></li>
                        <li>
							<fmt:formatNumber value="${reserv.ca_price1}" pattern="#,###"/>원</li>
                    </ul>
                </div>
                <div class="reser_tab select" onclick="click_tab2()">
                    <ul class="tab_contents">
                        <li><div class="circle1">
                                <img class="cir_checked" src="../img/icon/check_reserv.png">
                            </div>
                        </li>
                        <li>숙박</li>
                        <li><span> 예약가능 </span></li>
                        <li>
                        <c:choose>
							<c:when test="${usedate ==0 || usedate ==1 }">
								<fmt:formatNumber value="${reserv.ca_price2}" pattern="#,###"/>원
							</c:when>
							<c:otherwise>
								<fmt:formatNumber value="${reserv.ca_price2*usedate}" pattern="#,###"/>원
							</c:otherwise>
						</c:choose>
                        </li>
                    </ul>
                </div>
            </div>
            <div style="clear:both;"></div>
            <div class="usetime_box">
                <div id="time_bord">
                     <!-- <div class="timebox none">00:00</div>
                <div class="timebox none">01:00</div>
                <div class="timebox none">02:00</div>
                <div class="timebox none">03:00</div>
                <div class="timebox none">04:00</div>
                <div class="timebox none">05:00</div>
                <div class="timebox none">06:00</div>
                <div class="timebox none">07:00</div>
                <div class="timebox none">08:00</div>
                <div class="timebox none">09:00</div>
                <div class="timebox none">10:00</div>
                <div class="timebox none">11:00</div>
                <div class="timebox none">12:00</div>
                <div class="timebox none">13:00</div> -->
                <div class="timebox">14:00</div>
                <div class="timebox">15:00</div>
                <div class="timebox">16:00</div>
                <div class="timebox">17:00</div>
                <div class="timebox">18:00</div>
                <div class="timebox">19:00</div>
                <div class="timebox">20:00</div>
                <div class="timebox">21:00</div>
                <div class="timebox">22:00</div>
                <div class="timebox">23:00</div>
                <div class="timebox">24:00</div>
                </div>
            </div>
            <div class="reserv_input_box">
                <h1>필수 입력 사항</h1>
                <div style="clear: both;"></div>
                <div class="reserv_input1">
                    <div class="rev_name_box">
                        <span>예약자명</span>
                        <input type="text" value="${mname}" readonly="readonly" placeholder="예약자명을 입력하세요.">
                    </div>
                    <img src="../img/icon/check_reserv.png">
                    <div class="rev_ment_box">
                        <span>
                                예약자명은 실명 기입해주세요.
                        </span>
                    </div>
                </div>
                <div style="clear: both;"></div>
                <div class="reserv_input1">
                    <div class="rev_name_box">
                        <span>휴대전화</span>
                        <input type="text" placeholder="휴대전화 번호를 입력하세요.">
                    </div>
                    <img src="../img/icon/check_reserv.png">
                    <div class="rev_ment_box">
                        <span>
                                입력한 휴대전화 번호는 <span >안심번호로 변경</span>되어 숙소에 전달됩니다.
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="reser_cal2">
            <div class="date_box_area">
                <div class="date_box">
                    <div class="date_title">
                        <h1>체크인</h1>
                        <h1>체크아웃</h1>
                    </div>
                    <div class="date_content">
                        <input type="text" id="search_checkin" name="checkin" value="${checkin}" readonly> 
                        <img class="img_arrow" src="../img/icon/arrow2.png">
                        <input type="text" id="search_checkout" name="checkout" value="${checkout}" readonly>
                    </div>
                    <div class="date_line">
                    	<span id="view_enter_time"></span>
                    	<span id="view_out_time"></span>
                    </div>
                    <div class="stay_dates_box">
                            <h1>이용기간</h1>
                            <h1 id="stay_dates">1박2일</h1>
                    </div>
                </div>
            </div>

            <div style="clear: both;"></div>
            <div class="receipt_box">
                <div class="receipt_row1">
                    <span>적립 포인트</span>
                    <span>+0P</span>
                </div>
                <div class="receipt_row2">
                    <span>※ 결제 금액이 1만원 미만 또는 당일예약일 경우 포인트가 적립되지 않습니다.</span>
                    <br><span>※ 포인트는 로그인 후 결제할 경우에 적립됩니다.</span>
                </div>
                
                <div class="date_line"></div>
                <div class="receipt_row3">
                    <span>결제금액</span>
                    <span id='payment_amount'>530000</span>
                </div>
            </div>
            <div id="btn_payment" class="payment">
                결제하기
            </div>
        </div>
    </div>
    
    <form id="reserv_form" action="reservproc.do" method="POST">
    	<input type="hidden" id="st_id"		name="st_id" value="${st_id}">
	    <input type="hidden" id="ca_idx" 	name="ca_idx" value="${reserv.ca_idx}">
	    <input type="hidden" id="checkin"	name="checkin" value="${checkin}">
	    <input type="hidden" id="checkout"  name="checkout" value="${checkout}">
	    <input type="hidden" id="enter_time" name="enter_time" value="">
	    <input type="hidden" id="out_time"  name="out_time" value="">
	    <input type="hidden" id="price"		name="price" value="">
    </form>
    
    <input type="hidden" id="ca_use_time" value="${reserv.ca_use_time}">
    <input type="hidden" id="ca_enter_time" value="${reserv.ca_enter_time}" >
    <input type="hidden" id="ca_out_time" value="${reserv.ca_out_time}">
    <input type="hidden" id="ca_price1" value="${reserv.ca_price1}">
    <c:choose>
		<c:when test="${usedate ==0 || usedate ==1 }">
			<input type="hidden" id="ca_price2" value="${reserv.ca_price2}">
		</c:when>
		<c:otherwise>
			<input type="hidden" id="ca_price2" value="${reserv.ca_price2*usedate}">
		</c:otherwise>
	</c:choose>
    <div style="clear:both;"></div>
    <jsp:include page="../footer.jsp"></jsp:include>
</body>

</html>

<script>
	var usedate;
	
	usedate = Number(document.getElementById('search_usedate').value);
	document.getElementById("stay_dates").innerHTML= (usedate)+'박'+(usedate+1)+'일';
	
	
	document.getElementById("btn_payment").onclick = function(){
		document.getElementById("reserv_form").submit();
	}
	
</script>

<script>
    var reser_tab;
    var cir_checked;
    var timebox;
    var ca_enter_time;
    var ca_out_time;
    var	view_enter_time;
    var view_out_time;
    var payment_amount;
    var ca_price1;
    var ca_price2;
    var enter_time;
    var out_time;
    var price;
    
    reser_tab = document.getElementsByClassName('reser_tab');
    cir_checked = document.getElementsByClassName('cir_checked');
    time_bord = document.getElementById('time_bord');
    ca_use_time = document.getElementById('ca_use_time');
    ca_enter_time = document.getElementById('ca_enter_time');
    ca_out_time = document.getElementById('ca_out_time');
    view_enter_time = document.getElementById('view_enter_time');
    view_out_time = document.getElementById('view_out_time');
    payment_amount = document.getElementById('payment_amount');
    ca_price1 = document.getElementById('ca_price1');
    ca_price2 = document.getElementById('ca_price2');
    enter_time = document.getElementById('enter_time');
    out_time = document.getElementById('out_time');
    price= document.getElementById('price');
    
    var set_enter_out_time = function(enter, out){
    	if(enter == ""){
    		view_enter_time.innerHTML = ''; 
        	view_out_time.innerHTML = '';
        	enter_time.value ='';
        	out_time.value ='';
    	} else {
	    	view_enter_time.innerHTML = enter+':00시 이후'; 
	    	view_out_time.innerHTML = out+':00시 이전';
	    	enter_time.value =enter;
	    	out_time.value =out;
    	}
    }
    
    var click_tab1 = function(){

   		reser_tab[0].className='reser_tab select';
        reser_tab[1].className='reser_tab';
        cir_checked[0].style.display="block";
        cir_checked[1].style.display="none";
        time_bord.style.display="block";

        if(typeof stime != 'undefined' && stime!=0){
        	set_enter_out_time(stime,etime);
        } else {
        	set_enter_out_time('','');
        }
        
        document.getElementById("stay_dates").innerHTML= '1일';
        payment_amount.innerHTML = Number(ca_price1.value).toLocaleString()+'원';
        price.value= ca_price1.value;

    }
    var click_tab2 = function(){
        reser_tab[0].className='reser_tab';
        reser_tab[1].className='reser_tab select';
        cir_checked[0].style.display="none";
        cir_checked[1].style.display="block";
        time_bord.style.display="none";
        
        set_enter_out_time(ca_enter_time.value, ca_out_time.value);
        
        document.getElementById("stay_dates").innerHTML= (usedate)+'박'+(usedate+1)+'일';
        payment_amount.innerHTML = Number(ca_price2.value).toLocaleString()+'원';
        price.value= ca_price2.value;
    }

    click_tab2();
    
    if(usedate > 1){
    	reser_tab[0].style.display="none";
    	reser_tab[1].style.width = "600px";
    }
    
</script>
<script>
    var nowHours;
    var timeboxs;
    var startTime;
    var ca_use_time;
    var stime =0;
    var etime =0;
    
    nowHours = new Date().getHours();
    timeboxs = document.getElementsByClassName('timebox');
    ca_use_time = document.getElementById('ca_use_time');
    startTime = 14;

    var timeOnclcik = function(a,b){
       
        return function(){
            var startpos = a;
            var paintcount = Number(b);
            
            console.log('startpos:'+startpos);
            console.log('paintcount:'+paintcount);
            
            stime = startTime+startpos;
            console.log(stime+paintcount);
            etime = (stime+paintcount) > 24 ? 24:Number(stime+paintcount);
            
            view_enter_time.innerHTML = stime+':00시 이후'; 
            view_out_time.innerHTML = etime+':00시 이전'
            enter_time.value =ca_enter_time.value;
            out_time.value =ca_out_time.value;

            for(var i = 0; i<timeboxs.length; i++){
                if(i<=nowHours-startTime){
                    timeboxs[i].className = 'timebox notuse';
                } else {
                    timeboxs[i].className = 'timebox';
                }
            }
            
            for(var i =startpos; i<timeboxs.length; i++){
                if(paintcount == 0){
                    break;
                } else{
                    timeboxs[i].className = 'timebox select';
                    paintcount--;
                }
            }
        }
    }

    for(var i = 0; i<timeboxs.length; i++){
        if(i<=nowHours-startTime){
            timeboxs[i].className = 'timebox notuse';
        } else {
            timeboxs[i].onclick = timeOnclcik(i,ca_use_time.value);
        }
    }

</script>