<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <title>Document</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=e93e0b6aa39c656ccbb8d8fab1db0972"></script>
    <link href="../style/detail.css" rel="stylesheet" type="text/css">
    <style>
		/* calendar popup posiction*/
		.one_calender_box{
			position : absolute;
			top:0px;
			left: -34px;
	    	z-index: 999;
	    	display: none;
	}
</style>

</head>
<body>
	<jsp:include page="../header.jsp"></jsp:include>
	<form id="detail_form" action="hoteldetail.do" method="POST">
		<input type="hidden" id="detail_checkin" 	name="checkin" value="${checkin}">
		<input type="hidden" id="detail_checkout" 	name="checkout" value="${checkout}">
		<input type="hidden" id="st_id" 	name="st_id" value="${st_id}">
		<input type="hidden" id="st_name" 	name="st_name"  value="${st_name}">
		<input type="hidden" id="st_address" name="st_address"  value="${st_address}">
		<input type="hidden" id="st_phone" 	name="st_phone"  value="${st_phone}">
		<input type="hidden" id="avg_score" name="avg_score"  value="${avg_score}">
		<input type="hidden" id="re_count" 	name="re_count"  value="${re_count}">
		<input type="hidden" id="st_lat" 	name="st_lat" value="${st_lat}">
		<input type="hidden" id="st_long"	name="st_long" value="${st_long}">
		<input type="hidden" id="search_usedate" name="usedate" value="${usedate}">
	</form>
	
    <div class="warp" >
        <div class="phto_view">
            <div class="prev_btn">
                <img src="../img/icon/arrow_left.png">
            </div>
            <div class="next_btn">
                <img src="../img/icon/arrow_right.png">
            </div>
            <img src="../img/photo1.png">
            <div class="date_box_area">
                    <div class="date_box">
                        <div class="date_title">
                            <h1>체크인</h1>
                            <h1>체크아웃</h1>
                        </div>
                        <div class="date_content">
                            <input type="text" id="search_checkin" value="${checkin}" onclick="show_calendar()" readonly> 
                            <img class="img_arrow" src="../img/icon/arrow2.png">
                            <input type="text" id="search_checkout"  value="${checkout}" onclick="show_calendar()" readonly>
                        </div>
                        <div class="date_line"></div>
                        <div class="stay_dates_box">
                                <h1>이용기간</h1>
                                <h1 id="stay_dates">1박2일</h1>
                            </div>
                    </div>
                </div>
        </div>
        <div style="clear: both"></div>
        <div class="detail_col1">
            <div class="stay_name">
                <h1>${st_name}</h1>
            </div>
            <div class="address_phone">
                <h1>${st_address}</h1>
                <div class="line"></div>
                <h1>${st_phone}</h1>
            </div>
            <div>
                <div class="review_star">
                    <img src="../img/icon/star.png">
                    <img src="../img/icon/star.png">
                    <img src="../img/icon/star.png">
                    <img src="../img/icon/star.png">
                    <img src="../img/icon/star.png">
                
                </div>
                <div class="review">
                    <h1>후기 : ${re_count}개</h1>
                </div>
                <div style="clear: both"></div>
            </div>
           
            <div class="tab_box">
                <div class="tab">
                    객실정보
                </div>
                <div class="tab">
                    후기
                </div>
                
            </div>
            <div style="clear: both"></div>
            <div class="room_lsit_box">
                <ul>
                <c:forEach var='cabin' items="${cabinlist}" >
                
                    <li>
                        <div class="roominfo_box">
                            <div class="roominfo_col1">
                                <img src="/myhotel/img/${st_id}/${cabin.ca_img}">
                            </div>
                            <div class="roominfo_col2">
                            	<div>
                            		<h1 class="room_title">${cabin.ca_name}</h1>
                            	</div>
                            	<div>
                            		<h1 class="room_person">기준 ${cabin.ca_persons}명</h1>
                            	</div>
                                <div style="clear: both"></div>
                                <c:choose>
									<c:when test="${usedate ==0 || usedate ==1 }">
										<div class="room_price_box">
		                                    <h1>대실</h1>
		                                    <h1>최대 ${cabin.ca_use_time}시간</h1>
		                                    <h1><fmt:formatNumber value="${cabin.ca_price1}" pattern="#,###"/>원</h1>
		                                </div>
		                                <div class="roominfo_line"></div>
		                                <div class="room_price_box">
		                                    <h1>숙박</h1>
		                                    <h1>${cabin.ca_enter_time}:00 입실</h1>
		                                    <h1><fmt:formatNumber value="${cabin.ca_price2}" pattern="#,###"/>원</h1>
		                                </div>
									</c:when>
									<c:otherwise>
										<div class="room_price_box">
										</div>
		                                <div class="roominfo_line"></div>
		                                <div class="room_price_box">
		                                    <h1>숙박</h1>
		                                    <h1>${cabin.ca_enter_time}:00 입실</h1>
		                                    <h1><fmt:formatNumber value="${cabin.ca_price2*usedate}" pattern="#,###"/>원</h1>
		                                </div>
									</c:otherwise>
								</c:choose>
                                
                                <c:choose>
                                	<c:when test="${cabin.car_date == null}">
                                		<div class="room_reserv" onclick="reservation_move('${cabin.ca_idx}')" >
                                    		예약하기
                                		</div>
                                	</c:when>
                                	<c:otherwise>
                                		<div class="room_reserv rev_not">
                                    		예약마감
                                		</div>
                                	</c:otherwise>
                                </c:choose>
                                
                            </div>
                        </div>
                    </li>
                </c:forEach>
                </ul>
            </div>
        </div>
        <form id="reservation_move" action="reservation.do" method="GET">
        	<input type="hidden" id="detail_checkin" 	name="checkin" value="${checkin}">
			<input type="hidden" id="detail_checkout" 	name="checkout" value="${checkout}">
			<input type="hidden" id="st_id" 	name="st_id" value="${st_id}">
			<input type="hidden" id="ca_idx"	name="ca_idx" value="">
			<input type="hidden" id="usedate" name="usedate" value="${usedate}">
        </form>

        <div class="detail_col2">
            <div id="map" class="map"></div>
            <div style="clear: both"></div>
            <jsp:include page="../calender.jsp"></jsp:include>
        </div>
    </div>
    <div style="clear: both"></div>
    <jsp:include page="../footer.jsp"></jsp:include>
    
	<script>
		var tabs;

		tab = document.getElementsByClassName('tab');
		
		var setStyle = function(target, val1, val2, val3, val4){
			target.style.border = val1;
			target.style.backgroundColor = val2;
			target.style.borderBottom = val3;
			target.style.color = val4;
		}

		tab[0].onclick = function(){
			setStyle(tab[0], "2px solid #ff4800", "white", "0", "#ff4800");
			setStyle(tab[1], "2px solid #b3b3b3", "#e6e6e6", "2px solid #b3b3b3", "#808080");
		}

		tab[1].onclick = function(){
			setStyle(tab[0], "2px solid #b3b3b3", "#e6e6e6", "2px solid #b3b3b3", "#808080");
			setStyle(tab[1], "2px solid #ff4800", "white", "0", "#ff4800");
		}
		
		tab[0].onclick();
	</script>

    <script type="text/javascript">
    
    	//login check
    	var reservation_move = function(ca_idx){
    		
    		var mid = '${sessionScope.mid}';
    		
    		if(mid != '' && mid != 'undefined'){
    			$('#ca_idx').val(ca_idx);
        		document.getElementById('reservation_move').submit();
    		} else {
    			alert('로그인후 사용 가능합니다.');
    			location.href='/myhotel/joinus/login.do';
    		}
    	}
    </script>

    <script>
 		//calendar popup event
 		var usedate;
	
		usedate = Number(document.getElementById('search_usedate').value);
		document.getElementById("stay_dates").innerHTML= (usedate)+'박'+(usedate+1)+'일';
  
	    var show_calendar = function(){
			document.getElementById("one_calender_box").style.display="block";
		}
    	
	    var makeBetweenDate = function(date1, date2){
			var date1 = new Date(''+date1);
			var date2 = new Date(''+date2);
			var diff = (date2.getTime() - date1.getTime()) / (1000*60*60*24);
			return diff;
		}
	    
    	document.getElementById("cal_search_btn").onclick = function(){
    		var checkin_value= document.getElementById("checkin").value;
			var checkout_value= document.getElementById("checkout").value;
			
			document.getElementById("one_calender_box").style.display ="none";
			
			document.getElementById("detail_checkin").value = checkin_value;
			document.getElementById("detail_checkout").value = checkout_value;
			
			document.getElementById("search_usedate").value = makeBetweenDate(checkin_value, checkout_value);
    		document.getElementById("detail_form").submit();
    	}
    	
    </script>
    
    <script>
    	//map event
    	var st_lat =  document.getElementById('st_lat').value;
    	var st_long = document.getElementById('st_long').value;
    
	    var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
	    mapOption = { 
	    	 center: new daum.maps.LatLng(st_lat, st_long),
	    	//center: new daum.maps.LatLng(st_lat, st_long), // 지도의 중심좌표
	        level: 4 // 지도의 확대 레벨
	    };
	
		var map = new daum.maps.Map(mapContainer, mapOption); // 지도를 생성합니다
		
		var imageSrc = '/myhotel/img/icon/position.png', // 마커이미지의 주소입니다    
		    imageSize = new daum.maps.Size(24, 35) // 마커이미지의 크기입니다}; // 마커이미지의 옵션입니다. 마커의 좌표와 일치시킬 이미지 안에서의 좌표를 설정합니다.
		      
		// 마커의 이미지정보를 가지고 있는 마커이미지를 생성합니다
		var markerImage = new daum.maps.MarkerImage(imageSrc, imageSize),
		    markerPosition = new daum.maps.LatLng(st_lat, st_long); // 마커가 표시될 위치입니다
		
		// 마커를 생성합니다
		var marker = new daum.maps.Marker({
		    position: markerPosition, 
		    image: markerImage // 마커이미지 설정 
		});
		
		// 마커가 지도 위에 표시되도록 설정합니다
		marker.setMap(map);  
    </script>
</body>
</html>