String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
String.prototype.subFormat = function(len){return "0".string(len - this.length) + this;};
Number.prototype.subFormat = function(len){return this.toString().subFormat(len);};
Date.prototype.format = function(f) {
    if (!this.valueOf()) return "";
    var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
    var d = this;
    return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
        switch ($1) {
            case "yyyy": return d.getFullYear();
            case "yy": return (d.getFullYear() % 1000).subFormat(2);
            case "MM": return (d.getMonth() + 1).subFormat(2);
            case "dd": return d.getDate().subFormat(2);
            case "E": return weekName[d.getDay()];
            case "HH": return d.getHours().subFormat(2);
            case "hh": return ((h = d.getHours() % 12) ? h : 12).subFormat(2);
            case "mm": return d.getMinutes().subFormat(2);
            case "ss": return d.getSeconds().subFormat(2);
            case "a/p": return d.getHours() < 12 ? "오전" : "오후";
            default: return $1;
        }
    });
};

var basisDate = new Date();//화면에 보여질 달 
var toDay = new Date();//today의 Date를 세어주는 역할
var cells = new Array(); 
var farstDate;
var lastDate;
var saveDate1 = null;
var saveDate2 = null;

document.getElementById("checkin").value=toDay.format('yyyy-MM-dd');
document.getElementById("checkout").value=toDay.format('yyyy-MM-dd');

function initSave(){
    saveDate1 = null;
    saveDate2 = null;
    basisDate = new Date();
    buildCalendar();
    document.getElementById("checkin").value=toDay.format('yyyy-MM-dd');
    document.getElementById("checkout").value=toDay.format('yyyy-MM-dd');
    document.getElementById("info_ment").innerHTML="체크인 날짜를 선택하세요.";
}

function prevCalendar() {//이전 달
    basisDate = new Date(basisDate.getFullYear(), basisDate.getMonth() - 1, basisDate.getDate());
    buildCalendar(); //달력 cell 만들어 출력 
}

function nextCalendar() {//다음 달
    basisDate = new Date(basisDate.getFullYear(), basisDate.getMonth() + 1, basisDate.getDate());
    buildCalendar();//달력 cell 만들어 출력
}

function buildCalendar() {//현재 달 달력 만들기
    farstDate = new Date(basisDate.getFullYear(), basisDate.getMonth(), 1);
    //이번 달의 첫째 날,
    lastDate = new Date(basisDate.getFullYear(), basisDate.getMonth() + 1, 0);
    //이번 달의 마지막 날
    var tbCalendar = document.getElementById("calendar");
    var tbCalendarYM = document.getElementById("CalendarYM");
    tbCalendarYM.innerHTML = basisDate.getFullYear() + "년 " + (basisDate.getMonth() + 1) + "월";

    while (tbCalendar.rows.length > 2) {
        tbCalendar.deleteRow(tbCalendar.rows.length - 1);
    }

    var row = null;
    row = tbCalendar.insertRow();
    var cnt = 0

    for (i = 0; i < farstDate.getDay(); i++) {

        cell = row.insertCell();
        cnt = cnt + 1;
    }
    /*달력 출력*/
    for (i = 1; i <= lastDate.getDate(); i++) {

        cell = row.insertCell();
        cell.innerHTML = i;
       
        cnt = cnt + 1;

        cell.onclick = cellClickEvent;
        cell.cnt=cnt;
        cells[i]=cell;

        if (cnt % 7 == 1) {/*일요일 계산*/
            cell.style.color="#ff0000";
        }
        if (cnt % 7 == 0) {/* 1주일이 7일 이므로 토요일 구하기*/
            cell.style.color="#0085fe";
            row = calendar.insertRow();
        }
        //오늘의 날짜에 노란색 칠하기
        if(saveDate1== null){
            if (basisDate.format('yyyyMM') == toDay.format('yyyyMM') && i == toDay.getDate()) {
                cell.bgColor = "#FAF58C";
            }
        }
        
        var checkdate = new Date(basisDate.getFullYear()+"-"+(basisDate.getMonth()+1)+"-"+retrunNumber(i));

        paint_saveDate(checkdate, saveDate1, cell);
        paint_saveDate(checkdate, saveDate2, cell);
    }

    if(saveDate1== null){
        block_date(toDay,"before");
    } else{
        block_date(saveDate1,"before");
        block_date(saveDate2,"after");
    }
}

var retrunNumber = function(num){
    return num <10 ? '0'+num : num;
}

var cellClickEvent = function(){

    var selectDate = new Date(basisDate.getFullYear()+"-"+(basisDate.getMonth()+1)+"-"+retrunNumber(this.innerHTML));
    //console.log(selectDate.format('yyyyMMdd'));

    for(var i=1; i<cells.length; i++){
        cells[i].style.backgroundColor="white";
        cells[i].style.color="#808080";

        if (cells[i].cnt % 7 == 1) {/*일요일 계산*/
            cells[i].style.color="#ff0000";
        }
        if (cells[i].cnt % 7 == 0) {/* 1주일이 7일 이므로 토요일 구하기*/
            cells[i].style.color="#0085fe";
        }
        var checkdate = new Date(basisDate.getFullYear()+"-"+(basisDate.getMonth()+1)+"-"+retrunNumber(i));
        paint_saveDate(checkdate, saveDate1, cells[i]);
    }

    //console.log("saveDate1:"+saveDate1);
    if( saveDate1 == null){
        //console.log("save1");
        saveDate1 = selectDate;
        paint_saveDate(selectDate, saveDate1, this);
        block_date(selectDate,"before");
        document.getElementById("checkin").value=selectDate.format('yyyy-MM-dd');
        document.getElementById("info_ment").innerHTML="체크아웃 날짜를 선택해주세요.";
    }else {
        //console.log("save2");
        saveDate2 = selectDate;
        paint_saveDate(selectDate, saveDate2, this);
        block_date(saveDate1,"before");
        block_date(selectDate,"after");
        document.getElementById("checkout").value=selectDate.format('yyyy-MM-dd');
        document.getElementById("info_ment").innerHTML="모든날짜를 선택했습니다.";
    }
}

 var block_date = function(checkDate, direction){
    
    if(checkDate == null){
        return;
    }

    var check = checkDate.format("yyyyMM");
    var basis = basisDate.format("yyyyMM");
    //console.log('check:'+check+'- basis:'+basis +'결과:'+(check-basis));
    
    if( check==basis){
         //checkdate 와같은 달(화면에 보여지는달)
        if(direction == "before"){
            for(var i=1; i<checkDate.getDate();i++){
                cells[i].style.color="#EAEAEA";
                cells[i].onclick = null;
            }
        } else if(direction == "after"){
            for(var i=checkDate.getDate(); i<=lastDate.getDate();i++){
                cells[i].style.color="#EAEAEA";
                cells[i].onclick = null;
            }
        }
    } else if(direction == "before"){
        if(check-basis > 0){ //checkdate 보다 이전달
            for(var i=1; i<cells.length;i++){
                cells[i].style.color="#EAEAEA";
                cells[i].onclick = null;
            }
        } 
    } else if(direction == "after"){
        if(check-basis < 0){ //checkdate 보다 이후달
            for(var i=1; i<cells.length;i++){
                cells[i].style.color="#EAEAEA";
                cells[i].onclick = null;
            }
        }
    }
}

var paint_saveDate = function(checkdate, saveDate, cell){
    if(saveDate !=null){
        if( checkdate.format('yyyyMMdd') == saveDate.format('yyyyMMdd')){
            cell.style.color="white";
            cell.style.backgroundColor="#ff7200";
        }
    }
}

buildCalendar();