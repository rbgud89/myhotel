<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>로그인</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../style/login.css" />
</head>

<body>
	<jsp:include page="../header.jsp"></jsp:include>
    <div class="wrap">
        <div class="login_ment_box">
            <h1>어서</h1>
            <h1>로그인</h1>
            <h1> 하고</h1><br>
            <h1>원하는 호텔을 찾아보세요!</h1>
        </div>
        <form action="loginproc.do" method="POST">
            <div class="input_text">
                <input id="mid" name="mid" type="text" placeholder="아이디">
            </div>
            <div class="input_text">
                <input id="pwd" name="pwd" type="password" placeholder="비밀번호">
            </div>
            <div class="error_msg">${midx}</div>
            <div class="error_msg">${pwdx}</div>
            <div class="login_check_box">
                <input id="autologin" name="autologin" type="checkbox"><label>자동로그인</label>
            </div>
            <div style="clear: both"></div>
            <div class="login_btn_box">
                <input type="submit" value="로그인">
            </div>
        </form>
        <div class="repwd_box">
            <h3>비밀번호가 기억나지 않으시나요?</h3>
            <h3>비밀번호 재설정</h3>
        </div>
        <div class=""></div>
        <div class="adduser_box">
            <div class="adduser_ment_box">
                <h3>혹시, 아직</h3>
                <h3>회원</h3>
                <h3>이 아닌신가요?</h3>
            </div>
            <h3>회원가입하러 가기</h3>
        </div>
    </div>

    <div style="clear: both"></div>
    <jsp:include page="../footer.jsp"></jsp:include>
</body>

</html>