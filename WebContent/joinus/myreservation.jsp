<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!doctype html>
<html lang="ko">

<head>
    <!-- meta tags 필요 -->
    <meta charset="utf-8">
    <title>MyHotel로 예약하러 가자!</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="../style/myreservation.css" type="text/css" rel="stylesheet"/>
</head>
<body>
	
	<jsp:include page="../header.jsp"></jsp:include>
	<div style="clear: both"></div>
	<div class="wrap">
	<c:forEach var="mr" items="${mrlist}">
	<div class="myreserv_box">
		<div class="img_box">
			<img src="/myhotel/img/${mr.st_id}/${mr.ca_img}">
		</div>
		<div class="info">
			<div class="info_row1">
				<div>${mr.st_name}</div>
				<div>${mr.st_address}</div>
				<div>${mr.st_phone}</div>
			</div>
			<div class="info_row2">
				<div class="time_box">
					<span>체크인 </span>
					<span>${mr.checkin}</span><br>
					<span>체크아웃 </span>
					<span>${mr.checkout}</span><br>
					<span>입실시간 </span>
					<span>${mr.enter_time}:00</span><br>
					<span>퇴실시간 </span>
					<span>${mr.out_time}:00</span>
				</div>
				<div class="roominfo_box">
					<span>객실명</span>
					<span>${mr.ca_name}</span><br>
					<span>최대사용인원</span>
					<span>${mr.ca_persons}</span><br>
					<span>예약가격:</span>
					<span>${mr.price}</span><br>
					<span>예약일:</span>
					<span>${mr.re_date}</span>
				</div>
				<div class="btn_box">
					<div class="btn_boxs">
						<form action="myreserv_delproc.do" method="post">
							<input class="btn reserv_cancle" type="submit" value="예약취소">
							<input class="btn review" type="button" value="후기작성">
							<input type="hidden" name="mre_id" value="${mr.mre_id}">
						</form>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	
	</c:forEach>
	</div>
	<div style="clear: both"></div>
    <jsp:include page="../footer.jsp"></jsp:include>

</body>
</html>