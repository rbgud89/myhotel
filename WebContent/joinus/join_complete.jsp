<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../style/join_complete.css" />
</head>

<body>
	<jsp:include page="../header.jsp"></jsp:include>
    <div class="wrap">
        <div class="ment_box">
            <h1>회원가입이 완료 되었습니다!</h1>
            <h1>원하는 숙소를 지금 바로 찾아보세요!</h1>
        </div>
        <div class="complete_img">
            <img src="../img/complete.png">
        </div>
        <div class="complete_login" onclick="location.href='login.do'">
            로그인하기
        </div>
    </div>
    <jsp:include page="../footer.jsp"></jsp:include>
</body>

</html>