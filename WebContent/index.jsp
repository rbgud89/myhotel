<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="ko">

<head>
<!-- meta tags 필요 -->
<meta charset="utf-8">
<title>MyHotel로 예약하러 가자!</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="./style/index.css" type="text/css" rel="stylesheet">
<link href="./style/calendar.css" type="text/css" rel="stylesheet">
</head>
<style>
.one_calender_box {
	border: 0;
    display: block;
    }
</style>
<body>
	<jsp:include page="header.jsp"></jsp:include>

	<div style="clear: both"></div>
	<div class="warp">
		<div class="main_jumbo_box">
			<form id="fast_search_form" action="/myhotel/list/hotelsearch.do" method="GET">
				<input type="hidden" id="st_type" name="st_type" value=""> 
				<input type="hidden" id="fast_area" name="st_area_code" value="">
				<input type="hidden" id="usedate" name="usedate" value="">
				<div class="fast_search_box">
					<div class="fast_stay_box">
						<div class="fast_title_box">
							<h1>숙박유형</h1>
						</div>
						<div class="fast_stay_type" id="type_motel">
							<h1>모텔</h1>
							<img src="./img/icon/motel.png">
						</div>
						<div class="fast_stay_type" id="type_hotel">
							<h1>호텔</h1>
							<img src="./img/icon/hotel.png">
						</div>
						<div class="fast_stay_type" id="type_pension">
							<h1>펜션</h1>
							<img src="./img/icon/pension.png">
						</div>
					</div>
					<div class="fast_area_box">
						<div class="fast_title_box">
							<h1>지역</h1>
						</div>
						<div class="fast_area">
							<div class="fast_area_title">
								<h1>서울</h1>
							</div>
							<ul class="fast_area_list" id="alist1">
								<li data-value="a1">강남/역삼/삼성/논현</li>
								<li data-value="a2">잠실/신천</li>
								<li data-value="a3">구로/영등포/여의도/목동</li>
								<li data-value="a4">강서/화곡/까치산/양천</li>
								<li data-value="a5">성신여대/성북/월곡</li>
								<li data-value="a6">동대문/충무로/신당/약수/금호</li>
								<li data-value="a7">동묘/신설동/청량리/회기</li>
								<li data-value="a8">수유/미아</li>
								<li data-value="a9">상봉/중랑/면목</li>
							</ul>
							<ul class="fast_area_list" id="alist2">
								<li data-value="a10">서초/신사/방배</li>
								<li data-value="a11">천호/길동/둔촌</li>
								<li data-value="a12">신림/서울대/사당/금천/동작</li>
								<li data-value="a13">신촌/홍대/합정</li>
								<li data-value="a14">종로/대학로</li>
								<li data-value="a15">이태원/용산/서울역/명동</li>
								<li data-value="a16">건대/군자/구의</li>
								<li data-value="a17">장안동/답십리</li>
								<li data-value="a18">왕십리/성수</li>
								<li data-value="a19">태릉/노원/도봉/창동</li>
							</ul>
						</div>
					</div>
					<div class="fast_date_box">
						<div class="fast_title_box">
							<h1>날짜</h1>
						</div>
						<div class="fast_date">
							<div class="one_calender_box">
								<div class=c_cal1>
									<table id="calendar" class="one_calendar">
										<tr>
											<!-- label은 마우스로 클릭을 편하게 해줌 -->
											<td>
												<h1 class="cal_btn" onclick="prevCalendar()">
													◀</label>
											</td>
											<td id="CalendarYM" class="CalendarYM" colspan="5">
												yyyy년 m월</td>
											<td>
												<h1 class="cal_btn" onclick="nextCalendar()">
													▶</label>
											</td>
										</tr>
										<tr>
											<td class="su">SU</td>
											<td>MO</td>
											<td>TU</td>
											<td>WE</td>
											<td>TH</td>
											<td>FR</td>
											<td class="sa">SA</td>
										</tr>
									</table>
									<div class="c_line"></div>
									<div class="c_msg_box">
										<h1 id="info_ment">체크인 날짜를 선택하세요.</h1>
									</div>
								</div>

								<div class="c_cal2">
									<div style="clear: both"></div>
									<div class="dateinputbox">
										<h1>체크인</h1>
										<input type="text" id="checkin" name="checkin"
											value="new Date();" readonly>
										<h1>체크아웃</h1>
										<input type="text" id="checkout" name="checkout"
											value="new Date();" readonly>
									</div>

									<button class="initbnt" type="button" onclick="initSave();">
										<img class="img_fiter" src="./img/icon/filter2.png">날짜초기화
									</button>
									<button class="cal_search_btn" type="button" onclick="checkAndSubmit();">검색</button>
								</div>

							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<!--main_jumbo_box end-->

	<div style="clear: both"></div>
	<div class="main_middle_box">
		<div class="main_middle_title_box">
			<h1>어디로 가야하나 고민 중이신가요?</h1>
			<h2>지금 가장 인기있는 곳들을 소개합니다!</h2>
		</div>
		<div class="middle_row_box">
			<div class="best_box">
				<div class="best_img_box">
					<img src="./img/sub.jpg">
				</div>
				<div class="best_contents_box">
					<ul>
						<li>요즘 가장 핫한 최고 럭셔리 호텔!</li>
						<li>삼성 Boutique Hotel</li>
						<li>서울특별시 강남구 삼성동 168-16</li>
						<li><img src="./img/icon/star.png"> <img
							src="./img/icon/star.png"> <img src="./img/icon/star.png">
							<img src="./img/icon/star.png"> <img
							src="./img/icon/star.png"></li>
						<li>후기: 00개</li>
						<li><button type="button">상세정보 확인하러 가기</button>
					</ul>
				</div>

			</div>
			<div class="best_box">
				<div class="best_img_box">
					<img src="./img/sub2.jpg">
				</div>
				<div class="best_contents_box">
					<ul>
						<li>요즘 가장 핫한 최고 럭셔리 호텔!</li>
						<li>삼성 Boutique Hotel</li>
						<li>서울특별시 강남구 삼성동 168-16</li>
						<li><img src="./img/icon/star.png"> <img
							src="./img/icon/star.png"> <img src="./img/icon/star.png">
							<img src="./img/icon/star.png"> <img
							src="./img/icon/star.png"></li>
						<li>후기: 00개</li>
						<li><button type="button">상세정보 확인하러 가기</button>
					</ul>
				</div>
			</div>
		</div>
	</div>
	</div>

	<jsp:include page="footer.jsp"></jsp:include>

	<script>
		//숙소 타입 선택 이벤트
		var type_motel = document.getElementById("type_motel");
		var type_hotel = document.getElementById("type_hotel");
		var type_pension = document.getElementById("type_pension");		
		var alist1 = document.getElementById("alist1").getElementsByTagName("li");
		var alist2 = document.getElementById("alist2").getElementsByTagName("li");
		
		var st_type = document.getElementById("st_type");
		var fast_area = document.getElementById("fast_area");

		function stay_type_option(val, b1, b2, b3) {
			st_type.value = val;
			type_motel.style.border = b1;
			type_hotel.style.border = b2;
			type_pension.style.border = b3;
		}

		type_motel.onclick = function() {
			stay_type_option("MOTEL", "2px solid #ff7200", "0", "0");
		}
		type_hotel.onclick = function() {
			stay_type_option("HOTEL", "0", "2px solid #ff7200", "0");
		}
		type_pension.onclick = function() {
			stay_type_option("PENS", "0", "0", "2px solid #ff7200");
		}

		//지역 선택 이벤트
		add_alist_event(alist1);
		add_alist_event(alist2);

		function add_alist_event(list) {
			for (var i = 0; i < list.length; i++) {
				list[i].onclick = function() {
					for (var j = 0; j < alist1.length; j++) {
						alist1[j].style.border = "0";
					}

					for (var j = 0; j < alist2.length; j++) {
						alist2[j].style.border = "0";
					}
					this.style.border = "2px solid #ff7200";
					fast_area.value = this.dataset.value;
					console.log(this.innerHTML);
					//i가 왜 마지막인덱스 만 나오나
				}
			}
		}
		
		function checkAndSubmit(){
			if(st_type.value == ""){
				alert("숙박유형을 선택해 주세요.");
			} else if(fast_area.value == ""){
				alert("지역을 선택해 주세요.");
			} else{
				
				var makeBetweenDate = function(date1, date2){
					var date1 = new Date(''+date1);
					var date2 = new Date(''+date2);
					var diff = (date2.getTime() - date1.getTime()) / (1000*60*60*24);
					return diff;
				}
				
				var checkin = document.getElementById("checkin").value;
				var checkout = document.getElementById("checkout").value;

				document.getElementById("usedate").value = makeBetweenDate(checkin,checkout);
				
				document.getElementById("fast_search_form").submit();
			}
		}
	</script>
	<script type="text/javascript" src="calender.js"></script>
<script>
	
</script>
</body>

</html>