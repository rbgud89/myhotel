<%@page import="java.net.URLDecoder"%>
<%@page import="com.myhotel.dao.MemberDao"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<div class="main_titlebar">
	<div onclick="location.href='/myhotel/index.do'" class="title_log_box">
		<img src="/myhotel/img/myhotel_log.png">
	</div>
	<div class="titlebar_menu_box">
		<ul class="titlebar_menu">
			<li>내주변</li>
			<li>지역</li>
			<li>숙소 검색</li>
		</ul>
	</div>
	<div class="titlebar_submenu_box">
		<ul class=titlebar_submenu>
		<c:choose>
			<c:when test="${empty cookie.mid.value && empty sessionScope.mid}">
			<li><a href="/myhotel/joinus/join.do">회원가입</a></li>
            <li><a href="/myhotel/joinus/login.do">로그인</a></li>
			</c:when>
			<c:when test="${sessionScope.mid!=null}">
				<li>${sessionScope.mname}님 환영합니다.</li>
				<li id="mpoint">111</li>
            	<li><a href="/myhotel/joinus/myreservation.do">예약내역</a></li>
            	<li><a href="/myhotel/joinus/logout.do">로그아웃</a></li>
				<c:set var="mid" value="${sessionScope.mid}"></c:set>
				<c:set var="mname" value="${sessionScope.mname}"></c:set>
			</c:when>
			<c:when test="${cookie.mid.value!=null}">
				<li>${URLDecoder.decode(cookie.mname.value,'UTF-8')}님 환영합니다.</li>
				<li id="mpoint">111</li>
            	<li><a href="/myhotel/joinus/myreservation.do">예약내역</a></li>
            	<li><a href="/myhotel/joinus/logout.do">로그아웃</a></li>
				<c:set var="mid" value="${cookie.mid.value}"></c:set>
				<c:set var="mname" value=">${URLDecoder.decode(cookie.mname.value,'UTF-8')}"></c:set>
			</c:when>
		</c:choose>
		</ul>
	</div>
	<div class="searchbar_line" ></div>
</div>
<div class="main_hader_blank"></div>
<div style="clear: both;"></div>
<script>
$(document).ready(function () {
	var mid;
	mid = '${mid}';
	var point;
	console.log(mid);
	 $.ajax({

         url: '/myhotel/joinus/memberinfo.do',
         type: 'POST',
         data: {
             'mid': mid
         },
         dataType: 'JSON',
         success: function (data) {

             if (data.result == "exist") {
                 $('#mpoint').html(data.point.toLocaleString()+"pt");
             } else {
                 
             }
         },
         error: function (data) {
             //alert("서버 에러입니다. 관리자에게 문의주세요.");
         }
     });
});

</script>