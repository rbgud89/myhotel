<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/calendar.css" >


<div class="one_calender_box" id="one_calender_box">    
    <div class=c_cal1>
        <table id="calendar" class="one_calendar">
            <tr>
                <!-- label은 마우스로 클릭을 편하게 해줌 -->
                <td>
                    <h1 class="cal_btn" onclick="prevCalendar()">◀</label> 
                </td> 
                <td id="CalendarYM" class="CalendarYM"  colspan="5">
                    yyyy년 m월
                </td>
                <td>
                    <h1 class="cal_btn" onclick="nextCalendar()">▶</label>
                </td>
            </tr>
            <tr>
                <td class="su" >SU</td>
                <td >MO</td>
                <td >TU</td>
                <td >WE</td>
                <td >TH</td>
                <td >FR</td>
                <td class="sa">SA</td>
            </tr>
        </table>
        <div class="c_line"></div>
        <div class="c_msg_box">
            <h1 id="info_ment">체크인 날짜를 선택하세요.</h1>
        </div>
    </div>
    
    <div class="c_cal2">
        <div style="clear:both"></div>
        <div class="dateinputbox">
            <h1>체크인</h1>
            <input type="text" id="checkin" value="new Date();"readonly>
            <h1>체크아웃</h1>
            <input type="text" id="checkout" value="new Date();"readonly>
        </div>

        <button class="initbnt" type="button" onclick="initSave();">
        <img class="img_fiter" src="${pageContext.request.contextPath}/img/icon/filter2.png">날짜초기화</button>
        <button id="cal_search_btn" class="cal_search_btn" type="button" onclick="">확인</button>
    </div>

</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/calender.js" ></script>
