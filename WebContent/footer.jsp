<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<div class="footer">
	<div class="foot_top">
		<div class="foot_log_box">
			<h1>MyHotel</h1>
		</div>
		<div class="foot_top_menu_box">
			<ul class="foot_top_menu">
				<li>회사소개</li>
				<li>제휴광고문의</li>
				<li>인재채용</li>
				<li>이용약관</li>
				<li>개인정보방침</li>
			</ul>
		</div>
		<div style="clear: both"></div>
		<div class="foot_bottom">
			<div class="foot_bottom_menu_box">
				<ul>
					<li>(주)LGH</li>
					<li>대표이사: 이규형</li>
					<li>주소:서울시 강남구 테헤란로 211길 42 마이호텔빌딩</li>
					<li>메일: ask@myhotel.com</li>
				</ul>
			</div>
			<div class="foot_bottom_menu_box2">
				<ul>
					<li>홈페이지 제작: 기획 및 코딩-이규형</li>
					<li>디자인-장호승</li>
				</ul>
			</div>
		</div>
	</div>
</div>