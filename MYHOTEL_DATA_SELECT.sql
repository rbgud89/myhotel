SELECT * FROM HOTEL_STAY A LEFT JOIN 
(SELECT * FROM HOTEL_STAY_REV_STATUS WHERE STR_DATE BETWEEN '2018-10-20' AND '2018-10-21' GROUP BY ST_ID) B
ON A.ST_ID = B.ST_ID 
WHERE A.ST_AREA_CODE = 'a13' AND STR_DATE IS NULL;

#숙소 정보 검색
SELECT *,A.ST_ID MAIN_ST_ID FROM HOTEL_STAY A LEFT JOIN 
(SELECT * FROM HOTEL_STAY_REV_STATUS WHERE STR_DATE BETWEEN '2018-10-20' AND '2018-10-21' GROUP BY ST_ID ORDER BY ST_ID DESC) B
ON A.ST_ID = B.ST_ID 
LEFT JOIN (SELECT ST_ID, CA_PRICE1, CA_PRICE2 FROM HOTEL_CABIN GROUP BY ST_ID) C
ON A.ST_ID = C.ST_ID
LEFT JOIN (SELECT ST_ID,COUNT(*) RE_COUNT,ROUND(AVG(RE_SCORE),1) AVG_SCORE FROM HOTEL_REVIEW GROUP BY ST_ID) D
ON A.ST_ID = D.ST_ID
WHERE A.ST_AREA_CODE = 'a13' AND STR_DATE IS NULL AND ST_TYPE='MOTEL' AND CA_PRICE2 BETWEEN 45000 AND 50000 ORDER BY AVG_SCORE ;

#방정보 검색
SELECT * FROM HOTEL_CABIN A LEFT JOIN
(SELECT * FROM HOTEL_CABIN_REV_STATUS WHERE CAR_DATE BETWEEN '2018-10-20' AND '2018-10-21' GROUP BY CA_IDX ) B
ON A.CA_IDX = B.CA_IDX
LEFT JOIN (SELECT * FROM HOTEL_CABIN_IMG GROUP BY ST_ID) C
ON A.CA_IDX = C.CA_IDX WHERE A.ST_ID = 'a13_coja';

#예약하기 화면
SELECT A.ST_ID, ST_NAME, ST_PHONE, ST_ADDRESS, B.CA_IDX, CA_NAME, CA_PERSONS, CA_USE_TIME, CA_ENTER_TIME, CA_OUT_TIME, CA_PRICE1, CA_PRICE2, CA_IMG
FROM HOTEL_STAY A
LEFT JOIN HOTEL_CABIN 
B ON A.ST_ID = B.ST_ID
LEFT JOIN (SELECT * FROM HOTEL_CABIN_IMG WHERE CA_IDX = 'a13_coja1' GROUP BY CA_IDX) C
ON B.CA_IDX = C.CA_IDX
WHERE B.CA_IDX = 'a13_coja1'

#예약 조회
SELECT
 MRE_ID, MID, A.ST_ID, A.CA_IDX, CHECKIN, CHECKOUT, ENTER_TIME, OUT_TIME, PRICE, RE_DATE, ST_NAME, ST_PHONE, ST_ADDRESS, CA_IMG, CA_NAME, CA_PERSONS 
FROM HOTEL_MEM_RESERVE A
LEFT JOIN HOTEL_STAY B 
ON A.ST_ID = B.ST_ID 
LEFT JOIN HOTEL_CABIN C
ON A.CA_IDX = C.CA_IDX
LEFT JOIN (SELECT * FROM HOTEL_CABIN_IMG GROUP BY ST_ID) D
ON A.CA_IDX = D.CA_IDX
WHERE MID='rbgud89';

SELECT * FROM HOTEL_MEM_RESERVE;

